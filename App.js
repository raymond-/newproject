import AppNavigator from 'ca-navigation/AppNavigator';
import React, { useEffect } from 'react';
import { PersistGate } from 'redux-persist/integration/react';
import ErrorBoundary from 'ca-component-container/ErrorBoundary';
import { Provider } from 'react-redux';
import { sagaMiddleware, store, persistor } from 'ca-config/Store';
import bootstrapSaga from 'ca-bootstrap/bootstrapSaga';
import Bootstrap from 'ca-bootstrap/Bootstrap/index';
import SplashScreen from 'react-native-splash-screen';
import codePush from 'react-native-code-push';

sagaMiddleware.run(bootstrapSaga);

function App() {
  useEffect(() => {
    SplashScreen.hide();
  }, []);

  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <ErrorBoundary>
          <Bootstrap>
            <AppNavigator />
          </Bootstrap>
        </ErrorBoundary>
      </PersistGate>
    </Provider>
  );
}
export default codePush(App);
