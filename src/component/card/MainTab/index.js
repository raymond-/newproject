import * as React from 'react';
import {
  View,
  TouchableWithoutFeedback,
  StyleSheet,
  Image,
  TouchableOpacity,
  Platform,
} from 'react-native';
import PropTypes from 'prop-types';
import { setLang } from 'ca-module-auth/authAction';
import { Size, Color } from 'ca-config/index';
import {
  Home,
  HomeActive,
  User,
  UserActive,
  AttendanceActive,
  Attendance,
  PayslipActive,
  Payslip,
} from 'ca-config/Svg';
import { useIsFocused } from '@react-navigation/native';
import { SafeAreaView } from 'react-native-safe-area-context';
import DeviceInfo from 'react-native-device-info';
import Svg, { G, Path, Defs, ClipPath } from 'react-native-svg';
import { store } from 'ca-config/Store';
import { PaySlip, VideoCamera } from 'ca-config/Image';
import { setisShowModalOptCameraOrBiometric } from 'ca-bootstrap/bootstrapAction';

export default function MainTab(props) {
  const {
    state: { routes, index },
    navigation,
  } = props;

  const { lang } = store.getState().auth;
  const { width } = store.getState().bootstrap.dimensions;
  const centerWidth = DeviceInfo.isTablet() ? width / 10 + 2 : width / 5 + 2;
  const isFocused = useIsFocused();

  React.useEffect(() => {
    if (isFocused) {
      setLang(lang);
    }
  }, [isFocused, lang]);

  const renderIcon = React.useCallback((idx, fill) => {
    if (idx === 3) {
      return fill === 'url(#inactiveColor)' ? <User /> : <UserActive />;
    }
    if (idx === 2) {
      return fill === 'url(#inactiveColor)' ? (
        <Attendance />
      ) : (
        <AttendanceActive />
      );
    }
    if (idx === 1) {
      return fill === 'url(#inactiveColor)' ? <Payslip /> : <PayslipActive />;
    }
    return fill === 'url(#inactiveColor)' ? <Home /> : <HomeActive />;
  }, []);

  // eslint-disable-next-line react/no-unstable-nested-components, react/function-component-definition, no-shadow
  const Curved = (props) => (
    <Svg
      width={80}
      height={40}
      viewBox="0 0 80 40"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}>
      <G clipPath="url(#clip0_5410_140013)">
        <Path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M232 68.5h-383V0H0c0 22.012 19.683 39.857 39.99 39.857C60.295 39.857 80 22.012 80 0h152v68.5z"
          fill="none"
        />
      </G>
      <Defs>
        <ClipPath id="clip0_5410_140013">
          <Path fill="none" d="M0 0H80V40H0z" />
        </ClipPath>
      </Defs>
    </Svg>
  );

  const renderRounded = React.useMemo(() => {
    const emergencyContainer = {
      position: 'absolute',
      left: (width - 73) / 2,
      bottom: 40,
    };

    return (
      <View
        style={[
          emergencyContainer,
          {
            backgroundColor: DeviceInfo.isTablet()
              ? Color.main.light.white
              : Color.transparent.light.transparent,
          },
        ]}>
        <View style={Styles.zIndex1}>
          <Curved width={centerWidth} height={centerWidth / 2} />
        </View>
        <View style={[Styles.emergencySpacer, { width: centerWidth }]} />
        {Platform.OS === 'ios' && (
          <View
            style={[
              Styles.emergencySpacerContainerIos,
              {
                backgroundColor: DeviceInfo.isTablet()
                  ? Color.main.light.white
                  : Color.transparent.light.transparent,
              },
            ]}>
            <View
              style={[
                Styles.emergencySpacerIos,
                {
                  height: centerWidth / 2,
                },
              ]}
            />
            <View
              style={[
                Styles.emergencySpacerIos,
                {
                  height: centerWidth / 2,
                },
              ]}
            />
          </View>
        )}
        <TouchableOpacity
          onPress={() => {
            store.dispatch(setisShowModalOptCameraOrBiometric(true));
          }}
          style={[
            Styles.emergencyButtonContainer,
            {
              width: centerWidth - 10,
              height: centerWidth - 10,
              borderRadius: (centerWidth - 10) / 2,
              top: -(centerWidth - 50) / 2,
            },
          ]}>
          <View
            style={{
              width: 100,
              height: 100,
              padding: 10,
              backgroundColor: Color.main.light.white, // Add
              alignItems: 'center',
              borderRadius: 50,
              position: 'absolute',
              shadowColor: Color.main.dark.white,
            }}>
            <Image
              source={VideoCamera}
              style={{
                width: centerWidth - 10,
                height: centerWidth - 10,
              }}
              resizeMode="cover"
            />
          </View>
        </TouchableOpacity>
      </View>
    );
  }, [centerWidth, width]);

  const renderMainView = React.useMemo(() => {
    return routes.map((item, idx) => {
      return (
        <TouchableWithoutFeedback
          key={item.key}
          onPress={() => {
            navigation.navigate(item.name);
          }}>
          <View style={Styles.iconContainer}>
            <View style={Styles.icon}>
              {renderIcon(
                idx,
                idx === index ? 'url(#activeColor)' : 'url(#inactiveColor)'
              )}
            </View>
          </View>
        </TouchableWithoutFeedback>
      );
    });
  }, [index, navigation, renderIcon, routes]);

  return (
    <SafeAreaView
      edges={['left', 'right']}
      style={DeviceInfo.isTablet() ? Styles.containerTab : Styles.container}>
      {renderMainView}
      {renderRounded}
    </SafeAreaView>
  );
}

const Styles = StyleSheet.create({
  container: {
    height: Size.isIphoneX ? 64 + 30 : 64,
    flexDirection: 'row',
    shadowColor: Color.main.dark.white,
    elevation: 4,
    position: 'absolute',
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: Color.main.light.white, // Add this line
  },
  containerTab: {
    height: Size.isIphoneX ? 64 + 30 : 64,
    flexDirection: 'row',
    justifyContent: 'center',
    paddingBottom: 6,
    shadowRadius: 2,
    shadowOffset: {
      width: 0,
      height: -3,
    },
    shadowColor: Color.main.dark.white,
    elevation: 4,
    backgroundColor: Color.main.light.white, // Add this line
  },
  iconContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: Size.isIphoneX ? 0 : 14,
    zIndex: 9,
  },
  icon: {
    marginBottom: 4,
  },
  emergencySpacer: {
    flex: 1,
    backgroundColor: Color.main.light.white,
  },

  emergencySpacerContainerIos: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    top: 0,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  emergencySpacerIos: {
    width: 1,
    backgroundColor: Color.main.light.white,
  },
  emergencyButtonContainer: {
    position: 'absolute',
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 9,
  },
  zIndex1: {
    zIndex: 1,
  },
});

MainTab.propTypes = {
  navigation: PropTypes.objectOf(Object).isRequired,
  state: PropTypes.objectOf(String).isRequired,
};
