import React from 'react';
import PropTypes from 'prop-types';

class ErrorBoundary extends React.Component {
  componentDidCatch(error, info) {
    console.log('stack', JSON.stringify(info));
    console.log('message', `${error.message}`);
    console.log(error);
  }

  render() {
    const { children } = this.props;
    return children;
  }
}

export default ErrorBoundary;

ErrorBoundary.propTypes = {
  children: PropTypes.node.isRequired,
};
