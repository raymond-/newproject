import React, { useState } from 'react';
import FastImage from 'react-native-fast-image';
import Shimmer from 'ca-component-generic/Shimmer';

function ImageComponent({
  source,
  style,
  resizeMode,
  shimmer = true,
  ...props
}) {
  const [isLoading, setIsLoading] = useState(false);

  const handleLoadStart = () => {
    setIsLoading(true);
  };

  const handleLoadEnd = () => {
    setIsLoading(false);
  };

  if (isLoading && shimmer) {
    return (
      <Shimmer style={style} visible={!isLoading}>
        <FastImage
          source={source}
          style={style}
          onLoadStart={handleLoadStart}
          onLoadEnd={handleLoadEnd}
          {...props}
        />
      </Shimmer>
    );
  }
  return (
    <FastImage
      source={source}
      style={style}
      onLoadEnd={handleLoadEnd}
      resizeMode={resizeMode}
      {...props}
    />
  );
}

export default ImageComponent;
