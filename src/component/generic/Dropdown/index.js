import { StyleSheet, TouchableHighlight, View } from 'react-native';
import React, { useState } from 'react';
import Color from 'ca-config/Color';
import { ChevronDown } from 'ca-config/Svg';
import { ScrollView } from 'react-native-gesture-handler';
import FastImage from 'react-native-fast-image';
import HorizontalLine from 'ca-component-lifesaver/HorizontalLine';
import PropTypes from 'prop-types';
import { Text } from '..';
import Size from 'ca-config/Size';

function Dropdown(props) {
  const { label, onChange, list, value, placeholder, style } = props;
  const [isActive, setIsActive] = useState(false);
  const paddingCondition = (i) => {
    return {
      paddingTop: i === 0 ? 13 : 16,
      paddingBottom: i === list?.length - 1 ? 4 : 6,
    };
  };

  const onToggleInput = () => {
    setIsActive(!isActive);
  };
  function renderInput() {
    return (
      <TouchableHighlight onPress={onToggleInput} underlayColor="transparent">
        <View style={Style.renderInputContainer}>
          <View style={Style.renderInputContent}>
            <FastImage
              source={value?.img || list[0]?.img}
              resizeMode="contain"
              style={Style.img}
            />
            <Text textStyle="medium">{value?.label || list[0]?.label}</Text>
          </View>
          <ChevronDown fill={Color.neutral.light.neutral40} />
        </View>
      </TouchableHighlight>
    );
  }

  function renderSelection() {
    return (
      <View style={Style.renderSelectionContainer}>
        <TouchableHighlight underlayColor="transparent" onPress={onToggleInput}>
          <View style={Style.renderSelectionHeader}>
            <Text
              textStyle="medium"
              color={Color.neutralLifeSaver.light.neutral20}>
              {placeholder}
            </Text>
            <ChevronDown fill={Color.neutral.light.neutral40} />
          </View>
        </TouchableHighlight>
        <View style={Style.mb5} height={1}>
          <HorizontalLine height={1} />
        </View>
        <ScrollView showsVerticalScrollIndicator={false}>
          {list?.map((element, i) => (
            <TouchableHighlight
              onPress={() => {
                onToggleInput();
                onChange(element);
              }}
              underlayColor="transparent">
              <View>
                <View style={[Style.listContainer, paddingCondition(i)]}>
                  <FastImage
                    source={element?.img}
                    style={Style.img}
                    resizeMode="contain"
                  />
                  <Text textStyle="medium">{element?.label}</Text>
                </View>
                <View height={1}>
                  <HorizontalLine height={1} />
                </View>
              </View>
            </TouchableHighlight>
          ))}
        </ScrollView>
      </View>
    );
  }
  return (
    <View style={style}>
      <Text
        size={Size.text.caption1.size}
        textStyle="semi"
        line={18}
        letterSpacing={0.5}
        style={Style.mb4}
        color={Color.mediumGray.light.mediumGray}>
        {label}
      </Text>
      {isActive ? renderSelection() : renderInput()}
    </View>
  );
}

const Style = StyleSheet.create({
  renderInputContainer: {
    borderColor: Color.grayInput.light.grayInput,
    borderWidth: 1,
    paddingVertical: 8,
    paddingHorizontal: 12,
    borderRadius: 16,
    backgroundColor: Color.whiteCard.light.color,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  renderInputContent: { flexDirection: 'row', alignItems: 'center' },
  img: { width: 22, height: 15, marginRight: 10 },
  renderSelectionContainer: {
    borderColor: Color.grayInput.light.grayInput,
    borderWidth: 1,
    borderRadius: 16,
    paddingBottom: 7,
    backgroundColor: Color.whiteCard.light.color,
    maxHeight: 210,
  },
  renderSelectionHeader: {
    paddingHorizontal: 12,
    paddingTop: 8,
    paddingBottom: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  mb5: { marginBottom: 5 },
  listContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: 12,
  },
  mb4: { marginBottom: 4 },
});

Dropdown.propTypes = {
  label: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  list: PropTypes.arrayOf(Object).isRequired,
  value: PropTypes.objectOf(Object).isRequired,
  placeholder: PropTypes.string.isRequired,
  style: PropTypes.objectOf(Object).isRequired,
};

export default Dropdown;
