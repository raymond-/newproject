import React from 'react';
import BottomSheet from 'ca-component-container/BottomSheet';
import { View, StyleSheet, Alert } from 'react-native';
import { trans } from 'ca-util/trans';
import { NAVIGATION } from 'ca-util/constant';
import ReactNativeBiometrics from 'react-native-biometrics';
import PropTypes from 'prop-types';
import Button from 'ca-component-generic/Button';
import locale from '../locale';

function ModalOptCameraOrBiometric(props) {
  const { isShow, onClosePress, lang, navigation } = props;

  return (
    <BottomSheet
      isVisible={isShow}
      swipeable={false}
      onRequestClose={onClosePress}>
      <View style={Style.container}>
        <Button
          block
          onPress={() => {
            ReactNativeBiometrics.isSensorAvailable().then((resObj) => {
              const { available } = resObj;
              if (available) {
                onClosePress();
                navigation.navigate(NAVIGATION.HOME.HomeFingerprint);
              } else {
                onClosePress();
                Alert.alert('no biometric detected');
              }
            });
          }}
          style={Style.button1}>
          {trans(locale, lang, 'fingerprint')}
        </Button>
        <Button
          block
          onPress={() => {
            onClosePress();
            navigation.navigate(NAVIGATION.HOME.HomeCamera);
          }}
          style={Style.button1}>
          {trans(locale, lang, 'camera')}
        </Button>
      </View>
    </BottomSheet>
  );
}

const Style = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 70,
  },

  button1: {
    marginBottom: 24,
  },
});

ModalOptCameraOrBiometric.defaultProps = {
  isShow: false,
  onClosePress: () => {},
  lang: 'en',
};

ModalOptCameraOrBiometric.propTypes = {
  isShow: PropTypes.bool,
  onClosePress: PropTypes.func,
  lang: PropTypes.string,
  navigation: PropTypes.objectOf(Object).isRequired,
};

export default ModalOptCameraOrBiometric;
