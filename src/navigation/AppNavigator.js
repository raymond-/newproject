import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { navigationRef } from 'ca-bootstrap/bootstrapNavigation';
import { NAVIGATION } from 'ca-util/constant';
import { createStackNavigator } from '@react-navigation/stack';

// screen-list
import MainTabStack from 'ca-navigation/MainTabNavigator';
// import AuthStack from 'ca-module-auth/authNavigation';
import HomeStack from 'ca-module-home/homeNavigation';
import { HomeCamera, HomeFingerprint } from 'ca-module-home/screen';
import LoginStack from 'ca-module-login/loginNavigation';
import {
  PaySlipMainStack,
  PaySlipDetilStack,
  PaymentProcessStack,
  PaymentFinishStack,
} from 'ca-module-payslip/paySlipNavigation';
import NotificationStack from 'ca-module-notification/NotifNavigation';
import { store } from 'ca-config/Store';
import { setActivity } from 'ca-bootstrap/bootstrapAction';

const Stack = createStackNavigator();

function AppNavigator() {
  const routeNameRef = React.useRef();

  return (
    <NavigationContainer
      ref={navigationRef}
      onReady={() => {
        routeNameRef.current = navigationRef.current.getCurrentRoute().name;
      }}
      onStateChange={() => {
        const previousRouteName = routeNameRef.current;
        const currentRouteName = navigationRef.current.getCurrentRoute().name;
        if (previousRouteName !== currentRouteName) {
          store.dispatch(setActivity('CHANGE_SCREEN', currentRouteName));
        }
      }}>
      <Stack.Navigator
        screenOptions={{
          headerShown: false,
        }}>
        <Stack.Screen name={NAVIGATION.LOGIN.Login} component={LoginStack} />

        {/* TABMAIN */}
        <Stack.Screen
          name={NAVIGATION.TABMAIN.TabMain}
          component={MainTabStack}
        />

        {/* HOME */}
        <Stack.Screen name={NAVIGATION.HOME.Home} component={HomeStack} />
        <Stack.Screen
          name={NAVIGATION.HOME.HomeCamera}
          component={HomeCamera}
        />
        <Stack.Screen
          name={NAVIGATION.HOME.HomeFingerprint}
          component={HomeFingerprint}
        />
        {/* PAYSLIP MAIN */}
        <Stack.Screen
          name={NAVIGATION.PAYSLIP.PaySlip}
          component={PaySlipMainStack}
        />

        {/* PAYSLIP DETIL */}
        <Stack.Screen
          name={NAVIGATION.PAYSLIP.PaySlipDetil}
          component={PaySlipDetilStack}
        />
        {/* PAYMENT PROCESS */}
        <Stack.Screen
          name={NAVIGATION.PAYSLIP.PaymentProcess}
          component={PaymentProcessStack}
        />
        {/* PAYMENT FINISH */}
        <Stack.Screen
          name={NAVIGATION.PAYSLIP.PaymentFinish}
          component={PaymentFinishStack}
        />

        {/* NOTIFICATION MAIN */}
        <Stack.Screen
          name={NAVIGATION.NOTIFICATION.Notification}
          component={NotificationStack}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default AppNavigator;
