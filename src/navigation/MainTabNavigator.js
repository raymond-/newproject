import * as React from 'react';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MainTab as MainTabCard } from 'ca-component-card/index';
import { HomeMain } from 'ca-module-home/screen/index';
import { ProfileMain } from 'ca-module-profile/screen/index';
import { NewsfeedMain } from 'ca-module-newsfeed/screen';
import { headerNavStyle } from 'ca-config/Navbarstyle';
import { NAVIGATION } from 'ca-util/constant';
import AttendanceMainStack from 'ca-module-attendance/attendanceNavigation';
import { PaySlipMainStack } from 'ca-module-payslip/paySlipNavigation';

const Tab = createBottomTabNavigator();

export default function MainTab() {
  return (
    <Tab.Navigator
      screenOptions={{
        headerShown: false,
        tabBarStyle: {
          position: 'absolute',
          backgroundColor: 'white',
        },
      }}
      // eslint-disable-next-line react/no-unstable-nested-components
      tabBar={(props) => {
        return <MainTabCard {...props} />;
      }}>
      <Tab.Screen
        name="HomeMain"
        component={HomeMain}
        initialParams={{ label: 'HomeMain' }}
      />
      <Tab.Screen
        name="Payslip"
        component={PaySlipMainStack}
        initialParams={{ label: 'PaySlip' }}
      />
      <Tab.Screen
        name={NAVIGATION.ATTENDANCE.AttendanceMain}
        component={AttendanceMainStack}
        initialParams={{ label: 'MessageMain' }}
      />
      <Tab.Screen
        name="ProfileMain"
        component={ProfileMain}
        initialParams={{ label: 'ProfileMain' }}
        options={headerNavStyle({ onPress: () => {}, title: 'Profile' })}
      />
    </Tab.Navigator>
  );
}
