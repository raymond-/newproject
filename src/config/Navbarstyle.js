import { BtnBack } from 'ca-config/Svg';
import Color from 'ca-config/Color';
import { TouchableOpacity } from 'react-native-gesture-handler';
import React from 'react';

export const headerNavStyle = ({ onPress = () => {}, title }) => ({
  headerShown: true,
  headerTitleAlign: 'center',
  headerTitleStyle: {
    fontWeight: '100',
  },
  headerStyle: {
    height: 100,
  },
  title,
  headerLeft: () => (
    <TouchableOpacity style={{ padding: 15 }} onPress={onPress}>
      <BtnBack width={24} height={24} fill={Color.main.dark.white} />
    </TouchableOpacity>
  ),
});
