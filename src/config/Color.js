export default {
  primary: {
    dark: {
      primary10: 'rgba(255, 92, 58, 1)',
      primary20: 'rgba(255, 134, 148, 1)',
      primary40: 'rgba(255, 109, 127, 1)',
      primary60: 'rgba(255, 81, 101, 1)',
      primary80: 'rgba(255, 74, 96, 1)',
      primary90: 'rgba(237, 26, 51, 1)',
    },
    light: {
      primary10: 'rgba(255, 92, 58, 1)',
      primary20: 'rgba(255, 134, 148, 1)',
      primary40: 'rgba(255, 109, 127, 1)',
      primary60: 'rgba(255, 81, 101, 1)',
      primary80: 'rgba(255, 74, 96, 1)',
      primary90: 'rgba(237, 26, 51, 1)',
    },
  },
  neutral: {
    dark: {
      neutral10: 'rgba(163, 165, 168, 1)',
      neutral20: 'rgba(213, 213, 213, 1)',
      neutral40: 'rgba(80, 82, 84, 1)',
      neutral60: 'rgba(35, 36, 37, 1)',
      neutral80: 'rgba(32, 32, 33, 1)',
      neutral90: 'rgba(3, 13, 27, 1)',
    },
    light: {
      neutral10: 'rgba(163, 165, 168, 1)',
      neutral20: 'rgba(213, 213, 213, 1)',
      neutral40: 'rgba(80, 82, 84, 1)',
      neutral60: 'rgba(35, 36, 37, 1)',
      neutral80: 'rgba(32, 32, 33, 1)',
      neutral90: 'rgba(3, 13, 27, 1)',
    },
  },
  secondary: {
    dark: {
      secondary10: 'rgba(253, 214, 163, 0.3)',
      secondary20: 'rgba(253, 214, 163, 1)',
      secondary30: 'rgba(255, 162, 0, 1)',
      secondary40: 'rgba(252, 198, 126, 1)',
      secondary60: 'rgba(251, 189, 108, 1)',
      secondary80: 'rgba(251, 181, 89, 1)',
      secondary90: 'rgba(250, 173, 71, 1)',
      secondary100: 'rgba(255, 156, 39, 1)',
    },
    light: {
      secondary10: 'rgba(253, 214, 163, 0.3)',
      secondary20: 'rgba(253, 214, 163, 1)',
      secondary30: 'rgba(255, 162, 0, 1)',
      secondary40: 'rgba(252, 198, 126, 1)',
      secondary60: 'rgba(251, 189, 108, 1)',
      secondary80: 'rgba(251, 181, 89, 1)',
      secondary90: 'rgba(250, 173, 71, 1)',
      secondary100: 'rgba(255, 156, 39, 1)',
    },
  },
  success: {
    dark: {
      success10: 'rgba(77, 185, 72, 1)',
      success20: 'rgba(129, 209, 18, 1)',
      success30: 'rgba(29, 183, 174, 1)',
      success40: 'rgba(102, 179, 13, 1)',
      success60: 'rgba(79, 150, 9, 1)',
      success70: 'rgba(196, 230, 214, 0.8)',
      success80: 'rgba(79, 150, 9, 1)',
      success90: 'rgba(43, 100, 3, 1)',
      Success100: 'rgba(0, 216, 145, 0.2)',
    },
    light: {
      success10: 'rgba(77, 185, 72, 1)',
      success20: 'rgba(129, 209, 18, 1)',
      success30: 'rgba(29, 183, 174, 1)',
      success40: 'rgba(102, 179, 13, 1)',
      success60: 'rgba(79, 150, 9, 1)',
      success70: 'rgba(196, 230, 214, 0.8)',
      success80: 'rgba(79, 150, 9, 1)',
      success90: 'rgba(43, 100, 3, 1)',
      Success100: 'rgba(0, 216, 145, 0.2)',
    },
  },
  main: {
    dark: {
      white: 'rgba(0, 0, 0, 1)',
      black: 'rgba(255, 255, 255, 1)',
    },
    light: {
      white: 'rgba(255, 255, 255, 1)',
      black: 'rgba(0, 0, 0, 1)',
    },
  },
  ternary: {
    dark: {
      ternary10: 'rgba(28, 103, 246, 1)',
    },
    light: {
      ternary10: 'rgba(28, 103, 246, 1)',
    },
  },
  backgroundColor: {
    dark: {
      default10: 'rgba(249, 249, 255, 1)',
    },
    light: {
      default10: 'rgba(249, 249, 255, 1)',
    },
  },
  grayTitleButton: {
    dark: {
      grayTitleButton: 'rgba(112, 116, 120, 1)',
    },
    light: {
      grayTitleButton: 'rgba(112, 116, 120, 1)',
    },
  },
  buttonGradient: {
    light: {
      buttonGradient0: 'rgba(242, 93, 99, 1)',
      buttonGradient1: 'rgba(237, 28, 36, 1)',
    },
    dark: {
      buttonGradient0: 'rgba(242, 93, 99, 1)',
      buttonGradient1: 'rgba(237, 28, 36, 1)',
    },
  },
  grayButton: {
    dark: {
      grayButton: 'rgba(213, 216, 219, 1)',
    },
    light: {
      grayButton: 'rgba(213, 216, 219, 1)',
    },
  },
  transparent: {
    dark: {
      transparent: 'transparent',
    },
    light: {
      transparent: 'transparent',
    },
  },
  mediumGray: {
    dark: {
      mediumGray: 'rgba(102, 107, 111, 1)',
    },
    light: {
      mediumGray: 'rgba(102, 107, 111, 1)',
    },
  },
};
