/* eslint-disable import/extensions */
export { default as Background } from 'ca-asset/image/png/Background-2.png';

// HOME
export { default as Leaves } from 'ca-asset/image/png/Leaves.png';
export { default as Attendance } from 'ca-asset/image/png/Attendance.png';
export { default as PaySlip } from 'ca-asset/image/png/PaySlip.png';
export { default as DefaultPhotoAttendance } from 'ca-asset/image/png/DefaultPhotoAttendance.png';
export { default as Presents } from 'ca-asset/image/png/Presents.png';
export { default as Late } from 'ca-asset/image/png/Late.png';
export { default as Absent } from 'ca-asset/image/png/Absent.png';
export { default as VideoCamera } from 'ca-asset/image/png/video-camera.png';
export { default as Scan } from 'ca-asset/image/png/Scan.png';
export { default as ScanKK } from 'ca-asset/image/png/ScanKK.png';
export { default as ThumbPrint } from 'ca-asset/image/png/ThumbPrint.png';
