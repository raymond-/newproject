import * as CONST from 'ca-bootstrap/bootstrapConstant';
import * as STATE from 'ca-bootstrap/bootstrapInitialState';
import { persistReducer } from 'redux-persist';
import Persist from 'ca-config/Persist';
import moment from 'moment';
import { combineReducers } from 'redux';
import _ from 'lodash';
import { authReducer } from 'ca-module-auth/authReducer';
import { loginReducer } from 'ca-module-login/loginReducer';
import { paySlipReducer } from 'ca-module-payslip/paySlipReducer';
import { attendanceReducer } from 'ca-module-attendance/attendanceReducer';

export const activity = (state = _.cloneDeep(STATE.userActivity), action) => {
  const { payload, type } = action;
  const actions = {
    [CONST.SET_ACTIVITY]: () => ({
      userActivity: {
        type: payload.type,
        value: payload.value,
        date: moment(new Date()).format('YYYY-MM-DD HH:mm:ss'),
        dateDiff: moment(new Date()).diff(state.userActivity.date),
      },
      currentScreen: payload.type === 'CHANGE_SCREEN' ? payload.value : '',
      action: type,
    }),
    DEFAULT: () => state,
  };
  return (actions[type] || actions.DEFAULT)();
};

export const bootstrap = (
  state = _.cloneDeep(STATE.bootstrapInitialState),
  action
) => {
  const { payload, type } = action;
  const actions = {
    [CONST.SET_LOCATION]: () => ({
      ...state,
      latitude: payload.latitude,
      longitude: payload.longitude,
      action: type,
    }),

    [CONST.SET_LOADING]: () => ({
      ...state,
      loading: payload,
      action: type,
    }),

    [CONST.SET_TOASTMSG]: () => ({
      ...state,
      toastMsg: payload,
    }),

    [CONST.SET_DIMENSIONS]: () => ({
      ...state,
      dimensions: payload,
      action: type,
    }),
    [CONST.SET_IS_SHOW_MODAL_OPT_CAMERA_OR_BIOMETRIC]: () => ({
      ...state,
      isShowModalOptCameraOrBiometric: payload,
      action: type,
    }),

    DEFAULT: () => state,
  };
  return (actions[type] || actions.DEFAULT)();
};

export const localReducer = (
  state = _.cloneDeep(STATE.localInitialState),
  action
) => {
  const { payload, type } = action;
  const actions = {
    [CONST.SET_IS_ALREADY_LAUNCHED]: () => ({
      ...state,
      isAlreadyLaunched: payload,
      action: type,
    }),
    DEFAULT: () => state,
  };
  return (actions[type] || actions.DEFAULT)();
};

const bootstrapReducer = combineReducers({
  activity: persistReducer(Persist.activityConfig, activity),
  bootstrap,
  local: persistReducer(Persist.localConfig, localReducer),
  auth: persistReducer(Persist.authConfig, authReducer),
  login: persistReducer(Persist.authConfig, loginReducer),
  payslip: paySlipReducer,
  attendance: attendanceReducer,
});

export default bootstrapReducer;
