import { all } from 'redux-saga/effects';
import auth from 'ca-module-auth/authSaga';
import login from 'ca-module-login/loginSaga';
import payslip from 'ca-module-payslip/paySlipSaga';
import attendance from 'ca-module-attendance/attendanceSaga';

function* bootstrapSaga() {
  yield all([...auth, ...login, ...payslip, ...attendance]);
}

export default bootstrapSaga;
