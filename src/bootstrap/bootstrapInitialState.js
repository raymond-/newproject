import { Dimensions } from 'react-native';

export const userActivity = {
  userActivity: {
    type: '',
    value: '',
    date: '',
    dateDiff: '',
  },
  currentScreen: '',
};

export const bootstrapInitialState = {
  latitude: '',
  longitude: '',
  loading: false,
  isShowModalOptCameraOrBiometric: false,
  toastMsg: {},
  action: '',
  dimensions: {
    width: Dimensions.get('screen').width,
    height: Dimensions.get('screen').height,
  },
};

export const localInitialState = {
  isAlreadyLaunched: false,
};
