import React, { useCallback, useEffect, useRef } from 'react';
import {
  View,
  AppState,
  BackHandler,
  Alert,
  Dimensions,
  ActivityIndicator,
} from 'react-native';
import JailMonkey from 'jail-monkey';
// import { getLocation } from 'ca-util/location';
import { toastConfig } from 'ca-util/toast';
import PropTypes from 'prop-types';
import Toast from 'react-native-toast-message';
import { useMount } from 'ca-util/common';
import Color from 'ca-config/Color';
import style from './style';

function Bootstrap(props) {
  const {
    children,
    setActivity,
    // setLocation,
    toastMsg,
    setDimensions,
    loading,
    setDeviceId,
  } = props;
  const appState = useRef(AppState.currentState);

  Dimensions.addEventListener('change', (e) => {
    const { width, height } = e.window;
    setDimensions({ width, height });
  });

  const onAppStateChange = useCallback((nextAppState) => {
    if (
      appState.current?.match(/active/) &&
      (nextAppState === 'inactive' || nextAppState === 'background')
    ) {
      console.log('close app');
    } else if (JailMonkey.isJailBroken()) {
      Alert.alert(
        'Warning',
        'The device appears to be rooted/jailbroken, please use another device',
        [{ text: 'Close', onPress: () => BackHandler.exitApp() }],
        { cancelable: false }
      );
    } else {
      // event when application open
      console.log('it should get a config if apps open');
    }

    appState.current = nextAppState;
  }, []);

  useMount(() => {
    setDeviceId('08118627136');
    setActivity('OPEN_APP');
  });

  useEffect(() => {
    if (toastMsg?.type) {
      Toast.show({ type: toastMsg?.type, text1: toastMsg?.text1 });
    }
  }, [toastMsg]);

  useEffect(() => {
    const subscription = AppState.addEventListener('change', onAppStateChange);
    return () => {
      subscription.remove();
    };
  }, [onAppStateChange]);

  return (
    <View style={style.container}>
      {children}
      {loading && (
        <ActivityIndicator
          size="large"
          color={Color.primary.light.primary90}
          style={style.activityIndicator}
        />
      )}
      {toastMsg?.type && (
        <Toast position="top" bottomOffset={20} config={toastConfig} />
      )}
    </View>
  );
}

export default Bootstrap;

Bootstrap.propTypes = {
  children: PropTypes.node.isRequired,
  setActivity: PropTypes.func.isRequired,
  // setLocation: PropTypes.func.isRequired,
  toastMsg: PropTypes.objectOf(Object).isRequired,
  setDimensions: PropTypes.func.isRequired,
  setDeviceId: PropTypes.func.isRequired,
  loading: PropTypes.bool.isRequired,
};
