import {
  setActivity,
  setLocation,
  setDimensions,
} from 'ca-bootstrap/bootstrapAction';
import { setDeviceId } from 'ca-module-auth/authAction';
import { connect } from 'react-redux';
import Bootstrap from './View';

const mapStateToProps = (state) => ({
  toastMsg: state.bootstrap.toastMsg,
  loading: state.bootstrap.loading,
});

const mapDispatchToProps = {
  setActivity: (payload) => setActivity(payload),
  setLocation: (payload) => setLocation(payload),
  setDimensions: (payload) => setDimensions(payload),
  setDeviceId: (payload) => setDeviceId(payload),
};

export default connect(mapStateToProps, mapDispatchToProps)(Bootstrap);
