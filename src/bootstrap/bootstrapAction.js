import * as BOOTSTRAP from './bootstrapConstant';

export const setActivity = (type, value = '') => ({
  payload: {
    type,
    value,
  },
  type: BOOTSTRAP.SET_ACTIVITY,
});

export const setLocation = (payload) => ({
  type: BOOTSTRAP.SET_LOCATION,
  payload,
});

export const setLoading = (payload) => ({
  type: BOOTSTRAP.SET_LOADING,
  payload,
});

export const setToastMsg = (payload) => ({
  type: BOOTSTRAP.SET_TOASTMSG,
  payload,
});

export const setDimensions = (payload) => ({
  type: BOOTSTRAP.SET_DIMENSIONS,
  payload,
});

export const setIsAlreadyLaunched = (payload) => ({
  type: BOOTSTRAP.SET_IS_ALREADY_LAUNCHED,
  payload,
});

export const setisShowModalOptCameraOrBiometric = (payload) => ({
  type: BOOTSTRAP.SET_IS_SHOW_MODAL_OPT_CAMERA_OR_BIOMETRIC,
  payload,
});
