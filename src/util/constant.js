import Config from 'react-native-config';

export const {
  PERSIST_SECRET = '08118627136',
  BASE_URL = 'https://hrmsdemo.sopwer.net',
} = Config;

export const RESPONSE_STATUS = {
  ERR_NETWORK: 0,
  SUCCESS: 200,
  NEED_ACTION: 300,
  UNAUTHORIZED: 401,
  BAD_REQUEST: 400,
  FORBIDDEN: 403,
  ERROR: 500,
  ERR_BAD_RESPONSE: 502,
  ACCEPTED: 202,
};

export const RESPONSE_STATE = {
  INTERNAL_SERVER_ERROR: 'INTERNAL_SERVER_ERROR',
  ERR_NETWORK: 'ERR_NETWORK',
  BAD_REQUEST: 'BAD_REQUEST',
};

export const NAVIGATION = {
  AUTH: {
    Auth: 'Session',
    AuthLanding: 'AuthLanding',
    AuthLoad: 'Load',
  },
  TABMAIN: {
    TabMain: 'TabMain',
  },
  HOME: {
    Home: 'Home',
    HomeMain: 'HomeMain',
    HomeCamera: 'HomeCamera',
    HomeFingerprint: 'HomeFingerprint',
  },
  LOGIN: {
    Login: 'Login',
    LoginMain: 'LoginMain',
  },
  PAYSLIP: {
    PaySlip: 'PaySlip',
    PaySlipMain: 'PaySlipMain',
    PaySlipDetil: 'PaySlipDetil',
    PaymentProcess: 'PaymentProcess',
    PaymentFinish: 'PaymentFinish',
  },
  NOTIFICATION: {
    Notification: 'Notification',
    NotificationMain: 'NotificationMain',
  },
  ATTENDANCE: {
    Attendance: 'Attendance',
    AttendanceMain: 'AttendanceMain',
  },
};

export const API = {
  LOGIN: {
    login: '/api/method/login',
    logout: '/api/method/logout',
  },
  PAYSLIP: {
    payslip:
      '/api/resource/Salary Slip?fields=["employee_name","posting_date","status","gross_pay"]',
  },
  ATTENDANCE: {
    attendance:
      '/api/resource/Attendance?fields=["status","shift","in_time","out_time"]',
  },
};

export const TOAST = {
  type: {
    error: 'error',
    warning: 'warning',
    success: 'success',
    info: 'info',
  },
};

export const APP = {
  header: {
    height: 56,
  },
};
