import React from 'react';
import PropTypes from 'prop-types';
import CameraVision from 'ca-component-container/CameraVision';
import { trans } from 'ca-util/trans';
import style from './style';
import locale from './locale';

function HomeCamera(props) {
  const { navigation, lang, colorScheme } = props;

  return (
    <CameraVision
      baseTitle={trans(locale, lang, 'fotoDiri')}
      buttonTitle={trans(locale, lang, 'ulangi')}
      buttonSecondTitle={trans(locale, lang, 'gunakanFoto')}
      cameraFrame="selfie"
      navigation={navigation}
      onCaptured={(data) => {
        console.log(data);
        navigation.goBack();
      }}
    />
  );
}

export default HomeCamera;

HomeCamera.defaultProps = {
  colorScheme: 'light',
};

HomeCamera.propTypes = {
  navigation: PropTypes.objectOf(Object).isRequired,
  lang: PropTypes.string.isRequired,
  colorScheme: PropTypes.string,
};
