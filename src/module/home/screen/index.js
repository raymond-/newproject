export { default as HomeMain } from './HomeMain';
export { default as HomeCamera } from './HomeCamera';
export { default as HomeFingerprint } from './HomeFingerprint';
