import React from 'react';
import { Image, TouchableOpacity, View } from 'react-native';
import PropTypes from 'prop-types';
import Base from 'ca-component-container/Base';
import { HamburgerIcon, Notif } from 'ca-config/Svg';
import {
  Background,
  Leaves,
  Attendance,
  PaySlip,
  Presents,
  Late,
  Absent,
  DefaultPhotoAttendance,
} from 'ca-config/Image';
import Padder from 'ca-component-container/Padder';
import Text from 'ca-component-generic/Text';
import Size from 'ca-config/Size';
import CardImages from 'ca-module-home/components/CardImages';
import Color from 'ca-config/Color';
import ModalOptCameraOrBiometric from 'ca-component-modal/ModalOptCameraOrBiometric';
import { NAVIGATION } from 'ca-util/constant';
import style from './style';
import locale from './locale';

function HomeMain(props) {
  const {
    navigation,
    lang,
    colorScheme,
    isShowModalOptCameraOrBiometric,
    setisShowModalOptCameraOrBiometric,
  } = props;

  const handlePaySlip = () => {
    console.log('go to pay slip page');
    navigation.navigate(NAVIGATION.PAYSLIP.PaySlip);
  };

  const handleAttendance = () => {
    console.log('go to attendance page');
    navigation.navigate(NAVIGATION.ATTENDANCE.AttendanceMain);
  };

  const handleLeaves = () => {
    console.log('go to Leaves page');
  };

  const handleNotification = () => {
    console.log('go to Notification page');
    navigation.navigate(NAVIGATION.NOTIFICATION.Notification);
  };

  const defaultServicesArray = [
    {
      key: 0,
      image: Leaves,
      title: 'Leaves',
      subTitle: '12',
      subTitleColor: Color.primary.light.primary10,
      onPress: handleLeaves,
    },
    {
      key: 1,
      image: Attendance,
      title: 'Attendance',
      subTitle: '15',
      subTitleColor: Color.success.light.success30,
      onPress: handleAttendance,
    },
    {
      key: 2,
      image: PaySlip,
      title: 'PaySlip',
      subTitle: '17',
      subTitleColor: Color.secondary.light.secondary30,
      onPress: handlePaySlip,
    },
  ];

  const defaultTodayAttendaceArray = [
    {
      key: 0,
      image: Presents,
      innerImageText: '54',
      innerImageTextColor: Color.ternary.light.ternary10,
      title: 'Presents',
    },
    {
      key: 1,
      image: Late,
      innerImageText: '15',
      innerImageTextColor: Color.secondary.light.secondary30,
      title: 'Late',
    },
    {
      key: 2,
      image: Absent,
      innerImageText: '20',
      innerImageTextColor: Color.primary.light.primary10,
      title: 'Absent',
    },
  ];

  function renderHeader() {
    return (
      <Padder style={style.renderHeader.container}>
        <View style={style.renderHeader.hamburgerBellContainer}>
          <TouchableOpacity onPress={() => {}}>
            <HamburgerIcon />
          </TouchableOpacity>
          <TouchableOpacity onPress={handleNotification}>
            <Notif />
          </TouchableOpacity>
        </View>
        <View style={style.renderHeader.textContainer}>
          <Text
            textStyle="semi"
            size={Size.text.h6.size}
            letterSpacing={0.5}
            color={Color.main.light.white}>
            Hi Alex Smith
          </Text>
          <Text
            textStyle="bold"
            size={Size.text.h5.size}
            letterSpacing={0.5}
            color={Color.main.light.white}>
            Good Morning
          </Text>
        </View>
      </Padder>
    );
  }

  function renderChooseServices() {
    return (
      <View>
        <Text textStyle="bold" size={Size.text.body1.size} letterSpacing={0.5}>
          Please Choose Services
        </Text>
        <View style={style.renderChooseServices.cardImagesContainer}>
          {defaultServicesArray.map((val) => {
            return (
              <TouchableOpacity onPress={val?.onPress}>
                <CardImages
                  key={val?.key}
                  image={val?.image}
                  title={val?.title}
                  subTitle={val?.subTitle}
                  subTitleColor={val?.subTitleColor}
                />
              </TouchableOpacity>
            );
          })}
        </View>
      </View>
    );
  }

  function renderRecentLeave() {
    return (
      <View style={style.renderRecentLeave.container}>
        <View style={style.renderRecentLeave.headerTextContainer}>
          <Text
            textStyle="bold"
            size={Size.text.body1.size}
            letterSpacing={0.5}>
            Recent Leave Application
          </Text>
          <Text
            onPress={() => {}}
            textStyle="bold"
            color={Color.neutral.light.neutral10}
            letterSpacing={0.5}>
            See All
          </Text>
        </View>
        <View style={[style.renderRecentLeave.contentContainer]}>
          <View>
            <Image source={DefaultPhotoAttendance} />
          </View>
          <View style={style.renderRecentLeave.contentTextContainer}>
            <Text
              textStyle="bold"
              size={Size.text.body2.size}
              letterSpacing={0.5}>
              Alexa Smith
            </Text>
            <Text
              size={Size.text.caption1.size}
              color={Color.ternary.light.ternary10}
              letterSpacing={0.5}>
              27 Aug - 28 Aug, 2021
            </Text>
            <Text
              size={Size.text.caption1.size}
              color={Color.neutral.light.neutral10}
              letterSpacing={0.5}>
              Sick Leave Request
            </Text>
          </View>
          <View style={style.renderRecentLeave.btnContainer}>
            <TouchableOpacity
              style={style.renderRecentLeave.btnCancelContainer}>
              <Text
                align="center"
                textStyle="semi"
                size={Size.text.caption1.size - 1}
                color={Color.secondary.light.secondary30}
                letterSpacing={0.5}>
                Cancel
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={style.renderRecentLeave.btnApproveContainer}>
              <Text
                align="center"
                textStyle="semi"
                size={Size.text.caption1.size - 1}
                color={Color.success.light.success30}
                letterSpacing={0.5}>
                Approve
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  }

  function renderTodayAttendance() {
    return (
      <View>
        <Text textStyle="bold" size={Size.text.body1.size} letterSpacing={0.5}>
          Todays Attendance
        </Text>
        <View style={style.renderTodayAttendance.cardImagesContainer}>
          {defaultTodayAttendaceArray.map((val) => {
            return (
              <CardImages
                key={val?.key}
                image={val?.image}
                innerImageText={val?.innerImageText}
                innerImageTextColor={val?.innerImageTextColor}
                title={val?.title}
              />
            );
          })}
        </View>
      </View>
    );
  }

  return (
    <Base
      scrollEnabled
      topHeaderRadiusColor
      topHeaderRadius
      bgImage={Background}
      renderHeader={renderHeader()}
      isPaddingBottom={false}>
      <Padder style={style.pageContainer}>
        {renderChooseServices()}
        {renderRecentLeave()}
        {renderTodayAttendance()}
      </Padder>
      <ModalOptCameraOrBiometric
        isShow={isShowModalOptCameraOrBiometric}
        navigation={navigation}
        lang={lang}
        onClosePress={() => {
          setisShowModalOptCameraOrBiometric(false);
        }}
      />
    </Base>
  );
}

export default HomeMain;

HomeMain.defaultProps = {
  colorScheme: 'light',
};

HomeMain.propTypes = {
  navigation: PropTypes.objectOf(Object).isRequired,
  lang: PropTypes.string.isRequired,
  colorScheme: PropTypes.string,
  setisShowModalOptCameraOrBiometric: PropTypes.func.isRequired,
  isShowModalOptCameraOrBiometric: PropTypes.bool.isRequired,
};
