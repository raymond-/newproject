import { connect } from 'react-redux';
import { setisShowModalOptCameraOrBiometric } from 'ca-bootstrap/bootstrapAction';
import View from './View';

const mapStateToProps = (state) => ({
  lang: state.auth.lang,
  isShowModalOptCameraOrBiometric:
    state.bootstrap.isShowModalOptCameraOrBiometric,
});

const mapDispatchToProps = {
  setisShowModalOptCameraOrBiometric: (payload) =>
    // eslint-disable-next-line implicit-arrow-linebreak
    setisShowModalOptCameraOrBiometric(payload),
};

export default connect(mapStateToProps, mapDispatchToProps)(View);
