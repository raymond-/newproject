import Color from 'ca-config/Color';

export default {
  pageContainer: {
    backgroundColor: Color.backgroundColor.light.default10,
    paddingBottom: 200,
    flex: 1,
  },

  renderHeader: {
    container: { paddingTop: 24 },
    hamburgerBellContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    textContainer: { paddingVertical: 24 },
  },

  renderChooseServices: {
    cardImagesContainer: {
      flexDirection: 'row',
      justifyContent: 'space-evenly',
    },
  },

  renderRecentLeave: {
    container: { paddingVertical: 24 },
    headerTextContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
    },
    contentContainer: {
      flexDirection: 'row',
      justifyContent: 'space-between',
      backgroundColor: Color.main.light.white,
      paddingVertical: 24,
      paddingHorizontal: 16,
      borderRadius: 12,
    },
    contentTextContainer: {
      flex: 1,
      paddingLeft: 16,
      justifyContent: 'space-evenly',
    },
    btnContainer: {
      justifyContent: 'space-between',
    },
    btnCancelContainer: {
      backgroundColor: Color.secondary.light.secondary20,
      paddingHorizontal: 24,
      paddingVertical: 5,
      borderRadius: 16,
      marginBottom: 12,
    },
    btnApproveContainer: {
      backgroundColor: Color.success.light.success70,
      paddingHorizontal: 24,
      paddingVertical: 5,
      borderRadius: 16,
    },
  },

  renderTodayAttendance: {
    cardImagesContainer: {
      flexDirection: 'row',
      justifyContent: 'space-evenly',
    },
  },
};
