export default {
  renderContent: {
    padderContainer: {
      flex: 1,
      justifyContent: 'center',
    },
    contentContainer: {
      backgroundColor: 'white',
      justifyContent: 'center',
      alignItems: 'center',
      padding: 24,
      borderRadius: 16,
    },
    textPadding: { paddingVertical: 12 },
  },
};
