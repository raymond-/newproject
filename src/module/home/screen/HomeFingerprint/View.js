import React, { useState } from 'react';
import { Image, View } from 'react-native';
import PropTypes from 'prop-types';
import Base from 'ca-component-container/Base';
import Padder from 'ca-component-container/Padder';
import Button from 'ca-component-generic/Button';
import ReactNativeBiometrics from 'react-native-biometrics';
import { useMount } from 'ca-util/common';
import { ThumbPrint } from 'ca-config/Image';
import moment from 'moment';
import Size from 'ca-config/Size';
import Color from 'ca-config/Color';
import Text from 'ca-component-generic/Text';
import style from './style';
import locale from './locale';

function HomeFingerprint(props) {
  const { navigation, lang, colorScheme } = props;
  const [isSuccess, setSuccess] = useState(true);
  const [signTime, setSignTime] = useState(null);

  useMount(() => {
    ReactNativeBiometrics.isSensorAvailable().then((resObj) => {
      const { available } = resObj;
      if (available) {
        ReactNativeBiometrics.simplePrompt({
          promptMessage: 'Confirm fingerprint',
        })
          .then((res) => {
            if (res?.success) {
              const getSignTime = new Date();
              setSignTime(moment(getSignTime).format('hh:mm A'));
              setSuccess(false);
            } else {
              navigation.goBack();
            }
          })
          .catch(() => {
            console.log('biometrics failed');
          });
      }
    });
  });

  function renderContent() {
    return (
      <Padder style={style.renderContent.padderContainer}>
        <View style={style.renderContent.contentContainer}>
          <Image source={ThumbPrint} />
          <Text
            textStyle="regular"
            size={Size.text.body1.size}
            letterSpacing={0.5}
            style={style.renderContent.textPadding}>
            Please lift and rest your finger
          </Text>
          <Text
            textStyle="bold"
            color={Color.ternary.light.ternary10}
            size={Size.text.h4.size + 2}
            letterSpacing={0.5}
            style={style.renderContent.textPadding}>
            {signTime}
          </Text>
          <Button
            block
            disabled={isSuccess}
            onPress={() => {
              console.log(signTime);
              navigation.goBack();
            }}>
            Continue
          </Button>
        </View>
      </Padder>
    );
  }

  return (
    <Base
      backgroundColor={Color.backgroundColor.light.default10}
      title={
        <Text textStyle="bold" size={Size.text.h5.size - 2} letterSpacing={0.5}>
          Attendance
        </Text>
      }
      isPaddingBottom={false}
      onBackPress={() => {
        navigation.goBack();
      }}>
      {renderContent()}
    </Base>
  );
}

export default HomeFingerprint;

HomeFingerprint.defaultProps = {
  colorScheme: 'light',
};

HomeFingerprint.propTypes = {
  navigation: PropTypes.objectOf(Object).isRequired,
  lang: PropTypes.string.isRequired,
  colorScheme: PropTypes.string,
};
