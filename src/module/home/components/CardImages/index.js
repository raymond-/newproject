import { View, Image, StyleSheet } from 'react-native';
import propTypes from 'prop-types';
import React from 'react';
import Color from 'ca-config/Color';
import Size from 'ca-config/Size';
import Text from 'ca-component-generic/Text';

function CardImages(props) {
  const {
    image,
    innerImageText,
    innerImageTextColor,
    title,
    subTitle,
    subTitleColor,
  } = props;

  const imgWidth = 60;
  const imgHeight = 60;

  return (
    <View style={styles.container}>
      <View>
        <Image
          source={image}
          style={styles.relative}
          width={imgWidth}
          height={imgHeight}
        />
        <View
          style={[
            styles.innerTextContainer,
            { width: imgWidth, height: imgHeight },
          ]}>
          <Text
            style={{ color: innerImageTextColor }}
            textStyle="bold"
            size={Size.text.h5.size}
            letterSpacing={0.5}>
            {innerImageText}
          </Text>
        </View>
      </View>
      <Text
        style={styles.pV8}
        textStyle="bold"
        size={Size.text.body2.size - 1}
        letterSpacing={0.5}>
        {title}
      </Text>
      <Text
        textStyle="bold"
        size={Size.text.caption1.size}
        letterSpacing={0.5}
        color={subTitleColor}>
        {subTitle}
      </Text>
    </View>
  );
}
export default CardImages;

CardImages.defaultProps = {
  image: null,
  innerImageText: null,
  innerImageTextColor: null,
  title: null,
  subTitle: null,
  subTitleColor: null,
};

CardImages.propTypes = {
  image: propTypes.node,
  innerImageText: propTypes.string,
  innerImageTextColor: propTypes.string,
  title: propTypes.string,
  subTitle: propTypes.string,
  subTitleColor: propTypes.string,
};

const styles = StyleSheet.create({
  container: {
    backgroundColor: Color.main.light.white,
    width: 101,
    height: 125,
    borderRadius: 16,
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 24,
  },
  innerTextContainer: {
    position: 'absolute',
    top: 0,
    left: 0,
    alignItems: 'center',
    justifyContent: 'center',
  },
  relative: { position: 'relative' },
  pV8: { paddingVertical: 8 },
});
