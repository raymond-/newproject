import React from 'react';
import { Text, View, StyleSheet } from 'react-native';
import { IconChecklist, IconMoney, IconWallet, IconX } from 'ca-config/Svg';

import propTypes from 'prop-types';

function CardNotification(props) {
  const { category, desc } = props;

  let icon;
  let style;

  if (category === 'Request Accept') {
    icon = <IconChecklist />;
    style = styles.bgBlue;
  } else if (category === 'Leave Request') {
    icon = <IconX />;
    style = styles.bgOrange;
  } else if (category === 'Payment') {
    icon = <IconWallet />;
    style = styles.bgYellow;
  } else if (category === 'Promotion') {
    icon = <IconMoney />;
    style = styles.bgRed;
  } else {
    icon = <IconChecklist />;
    style = styles.bgOrange;
  }
  return (
    <View style={styles.card}>
      <View style={[styles.containerIcon, style]}>{icon}</View>
      <View>
        <Text style={styles.textTitle}>{category}</Text>
        <Text style={styles.textDesc}>{desc}</Text>
      </View>
    </View>
  );
}

export default CardNotification;

CardNotification.defaultProps = {
  category: 'Lorem Ipsum',
  desc: 'Lorem Ipsum........',
};

CardNotification.propTypes = {
  category: propTypes.string,
  desc: propTypes.string,
};

const styles = StyleSheet.create({
  card: {
    width: '100%',
    backgroundColor: 'white',
    height: 88,
    borderRadius: 15,
    padding: 16,
    alignItems: 'center',
    flexDirection: 'row',
    marginBottom: 15,
  },
  containerIcon: {
    height: 50,
    width: 50,
    borderRadius: 100,
    alignItems: 'center',
    justifyContent: 'center',
    marginRight: 20,
  },
  textTitle: {
    fontSize: 16,
    fontWeight: '700',
    color: 'rgba(36, 42, 55, 1)',
    marginBottom: 8,
  },
  textDesc: {
    fontSize: 14,
    fontWeight: '400',
    color: 'rgba(36, 42, 55, 1)',
  },
  bgRed: {
    backgroundColor: 'rgba(255, 162, 107, 1)',
  },
  bgYellow: {
    backgroundColor: 'rgba(255, 188, 0, 1)',
  },
  bgOrange: {
    backgroundColor: 'rgba(251, 138, 36, 1)',
  },
  bgBlue: {
    backgroundColor: 'rgba(28, 103, 246, 1)',
  },
});
