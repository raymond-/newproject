import React from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import Padder from 'ca-component-container/Padder';
import CardNotification from 'ca-module-notification/component/CardNotification';
import Base from 'ca-component-container/Base';
import Color from 'ca-config/Color';
import Size from 'ca-config/Size';
import { HumbergerAlignment } from 'ca-config/Svg';
import Text from 'ca-component-generic/Text';
import style from './style';

function NotificationMain(props) {
  const { navigation, lang, colorScheme } = props;

  const dataDummyNotification = [
    {
      id: 1,
      desc: 'halooo',
      category: 'Payment',
    },
    {
      id: 2,
      desc: 'halooo',
      category: 'Leave Request',
    },
    {
      id: 3,
      desc: 'halooo',
      category: 'Request Accept',
    },
    {
      id: 4,
      desc: 'halooo',
      category: 'Payment',
    },
    {
      id: 5,
      desc: 'halooo',
      category: 'Promotion',
    },
  ];

  return (
    <Base
      scrollEnabled
      title={
        <Text textStyle="bold" size={Size.text.h5.size - 2} letterSpacing={0.5}>
          Notification
        </Text>
      }
      onBackPress={() => {
        navigation.goBack();
      }}
      customLeftIcon={<HumbergerAlignment width={24} height={24} />}
      isPaddingBottom={false}>
      <Padder style={style.padderContainer}>
        <View style={style.mT20}>
          {dataDummyNotification.map((data) => (
            <CardNotification
              key={data.id}
              category={data.category}
              desc={data.desc}
            />
          ))}
        </View>
      </Padder>
    </Base>
  );
}

export default NotificationMain;

NotificationMain.defaultProps = {
  colorScheme: 'light',
};

NotificationMain.propTypes = {
  navigation: PropTypes.objectOf(Object).isRequired,
  lang: PropTypes.string.isRequired,
  colorScheme: PropTypes.string,
};
