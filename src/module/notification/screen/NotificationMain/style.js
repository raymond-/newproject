import Color from 'ca-config/Color';

export default {
  padderContainer: {
    backgroundColor: Color.backgroundColor.light.default10,
    flex: 1,
  },
  mT20: { marginTop: 20 },
};
