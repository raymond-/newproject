import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NAVIGATION } from 'ca-util/constant';
import * as Screen from './screen/index';

const Stack = createStackNavigator();
const { NOTIFICATION } = NAVIGATION;

function NotificationStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: false,
      }}>
      <Stack.Screen
        name={NOTIFICATION.NotificationMain}
        component={Screen.NotificationMain}
      />
    </Stack.Navigator>
  );
}

export default NotificationStack;
