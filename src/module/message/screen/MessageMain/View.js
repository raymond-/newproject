import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';
import Base from 'ca-component-container/Base';
import Padder from 'ca-component-container/Padder';
import style from './style';
import locale from './locale';

function MessageMain(props) {
  const { navigation, lang, colorScheme } = props;

  return (
    <Base>
      <Padder>
        <Text>Message Main</Text>
      </Padder>
    </Base>
  );
}

export default MessageMain;

MessageMain.defaultProps = {
  colorScheme: 'light',
};

MessageMain.propTypes = {
  navigation: PropTypes.objectOf(Object).isRequired,
  lang: PropTypes.string.isRequired,
  colorScheme: PropTypes.string,
};
