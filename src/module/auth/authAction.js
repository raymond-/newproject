import * as CONST from './authConstant';

export const setDeviceId = (payload) => ({
  type: CONST.SET_DEVICE_ID,
  payload,
});
export const setColorScheme = (payload) => ({
  type: CONST.SET_COLOR_SCHEME,
  payload,
});
export const setLang = (payload) => ({
  type: CONST.SET_LANG,
  payload,
});
export const setAuth = (payload) => ({
  type: CONST.SET_AUTH,
  payload,
});
export const setToken = (payload) => ({
  type: CONST.SET_TOKEN,
  payload,
});
export const setUserData = (payload) => ({
  type: CONST.SET_USER_DATA,
  payload,
});
export const setClearRefreshToken = (payload) => ({
  type: CONST.SET_CLEAR_REFRESH_TOKEN,
  payload,
});

// GET SAMPLE
export const getSample = (payload) => ({
  type: CONST.GET_SAMPLE,
  payload,
});
export const getSampleSuccess = (payload) => ({
  type: CONST.GET_SAMPLE_SUCCESS,
  payload,
});
export const getSampleFailed = (payload) => ({
  type: CONST.GET_SAMPLE_FAILED,
  payload,
});
export const setSampleData = (payload) => ({
  type: CONST.SET_EVENT_DETAIL_DATA,
  payload,
});
export const getSampleClear = (payload) => ({
  type: CONST.GET_SAMPLE_CLEAR,
  payload,
});
