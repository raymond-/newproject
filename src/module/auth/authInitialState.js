export const authInitialState = {
  lang: 'id',
  colorScheme: 'light',
  userData: {
    userId: '',
    deviceId: '',
    email: '',
    name: '',
    mobilePhoneNumber: '',
  },
  token: {
    refresh_token: '',
    token_type: 'bearer',
    access_token: '',
    expires_in: 0,
    error_description: null,
    error: null,
  },
};

// GET SAMPLE
export const getSampleInitialState = {
  getSampleFetch: false,
  getSampleParam: {},
  getSampleResponse: {},
  getSampleFailed: {
    message: '',
  },
};
