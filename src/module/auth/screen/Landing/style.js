export default {
  landingContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
  flex: { flex: 1 },
  mT36: { marginTop: 36 },
};
