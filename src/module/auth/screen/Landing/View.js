import React, { useCallback } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import { useMount } from 'ca-util/common';
import SplashScreen from 'react-native-splash-screen';
import { useFocusEffect } from '@react-navigation/native';
import { NAVIGATION } from 'ca-util/constant';
import style from './style';
import locale from './locale';

function Landing(props) {
  const {
    navigation,
    lang,
    colorScheme,
    setLang,
    dimensions,
    userData,
    setIsAlreadyLaunched,
    getSample,
    getSampleClear,
    getSampleResponse,
  } = props;

  useFocusEffect(
    useCallback(() => {
      setIsAlreadyLaunched(true);
      getSample();
      return () => {
        getSampleClear();
      };
    }, [getSample, getSampleClear, setIsAlreadyLaunched])
  );

  useMount(() => {
    SplashScreen.hide();
  });

  return (
    <View style={style.landingContainer}>
      <View style={[style.flex, style.mT36]}>
        <Text>Boiler Plate with codePush</Text>
        <Text>
          commmand : appcenter codepush release-react -a newproject/boilerplate
          -d Staging
        </Text>
      </View>
      <View style={style.flex}>
        <Text>This is a sample API Response</Text>
        <Text>userId : {getSampleResponse?.userId}</Text>
        <Text>id : {getSampleResponse?.id}</Text>
        <Text>title : {getSampleResponse?.title}</Text>
        <Text>completed : {getSampleResponse?.completed?.toString()}</Text>
        <TouchableOpacity
          onPress={() => {
            navigation.replace(NAVIGATION.TABMAIN.TabMain);
          }}
          style={{
            paddingVertical: 16,
            backgroundColor: 'salmon',
            borderRadius: 16,
            alignItems: 'center',
          }}>
          <Text>Go TabMain</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default Landing;

Landing.defaultProps = {
  colorScheme: 'light',
};

Landing.propTypes = {
  navigation: PropTypes.objectOf(Object).isRequired,
  lang: PropTypes.string.isRequired,
  colorScheme: PropTypes.string,
  setLang: PropTypes.func.isRequired,
  userData: PropTypes.objectOf(Object).isRequired,
  dimensions: PropTypes.objectOf(Object).isRequired,
  setIsAlreadyLaunched: PropTypes.func.isRequired,
  getSample: PropTypes.func.isRequired,
  getSampleClear: PropTypes.func.isRequired,
  getSampleResponse: PropTypes.objectOf(Object).isRequired,
};
