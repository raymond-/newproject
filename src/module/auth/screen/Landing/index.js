import { setIsAlreadyLaunched } from 'ca-bootstrap/bootstrapAction';
import { getSample, setLang, getSampleClear } from 'ca-module-auth/authAction';
import { connect } from 'react-redux';
import View from './View';

const mapStateToProps = (state) => ({
  lang: state.auth.lang,
  colorScheme: state.auth.colorScheme,
  userData: state.auth.userData,
  dimensions: state.bootstrap.dimensions,
  getSampleResponse: state.auth.getSampleResponse,
});

const mapDispatchToProps = {
  setLang: (payload) => setLang(payload),
  setIsAlreadyLaunched: (payload) => setIsAlreadyLaunched(payload),
  getSample: (payload) => getSample(payload),
  getSampleClear: () => getSampleClear(),
};

export default connect(mapStateToProps, mapDispatchToProps)(View);
