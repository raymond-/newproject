import { takeLatest, put, call } from 'redux-saga/effects';
import { RESPONSE_STATUS } from 'ca-util/constant';
import { getSampleApi } from 'ca-module-auth/authApi';
import * as CONST from 'ca-module-auth/authConstant';
import { getSampleFailed, getSampleSuccess } from 'ca-module-auth/authAction';

function* getSample(params) {
  try {
    const response = yield call(getSampleApi, params.payload);
    yield put(getSampleSuccess(response.data));
  } catch (error) {
    switch (error?.response?.status) {
      case RESPONSE_STATUS.BAD_REQUEST:
        yield put(getSampleFailed(error?.response?.data));
        break;
      case RESPONSE_STATUS.ERROR:
        yield put(getSampleFailed(error?.response?.data));
        break;
      default:
        yield put(getSampleFailed(error?.response?.data));
        break;
    }
  }
}

export default [takeLatest(CONST.GET_SAMPLE, getSample)];
