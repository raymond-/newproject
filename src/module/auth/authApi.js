import { api } from 'ca-bootstrap/bootstrapApi';
import { API } from 'ca-util/constant';

export const getSampleApi = (payload) => {
  return api.get(API.Sample.todo, payload);
};
