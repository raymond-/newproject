// Login.js
import React, { useState, useEffect, useRef } from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import style from './style';
import { NAVIGATION } from 'ca-util/constant';
import { DefaultPhotoAttendance } from 'ca-config/Image';
import { ScrollView } from 'react-native-gesture-handler';
import { Check, Edit } from 'ca-config/Svg';
const Profile = ({ navigation }) => {
  return (
    <ScrollView style={style.container}>
      <View style={style.wrapperCardProfile}>
        <View style={{ marginBottom: 20 }}>
          <Image source={DefaultPhotoAttendance} style={style.photoProfile} />
          <TouchableOpacity
            style={{ position: 'absolute', bottom: 20, right: -10 }}>
            <Edit />
          </TouchableOpacity>
        </View>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            justifyContent: 'space-around',
          }}>
          <Text style={style.name}>Axex Smith</Text>
          <Check />
        </View>
        <Text style={style.jobTitle}>UX Designer</Text>
        <Text style={style.idProfile}>ID:5025</Text>
      </View>
      <View style={style.wrapperCard}>
        <View style={style.card}>
          <Text style={style.titleCard}>22 Aug, 2020</Text>
          <Text style={style.subTitleCard}>Join Date</Text>
        </View>
        <View style={style.card}>
          <Text style={style.titleCard}>$2500.00</Text>
          <Text style={style.subTitleCard}>Net Salary</Text>
        </View>
      </View>
      <Text style={style.titleWrapperdCard}>Deducation</Text>
      <View style={style.wrapperCard}>
        <View style={[style.card, { minWidth: 101, minHeight: 101 }]}>
          <Text style={[style.titleCard, { color: '#1C67F6' }]}>$54,00</Text>
          <Text style={style.subTitleCard}>Basic</Text>
        </View>
        <View style={[style.card, { minWidth: 101, minHeight: 101 }]}>
          <Text style={[style.titleCard, { color: '#FFA200' }]}>15%</Text>
          <Text style={style.subTitleCard}>Tax</Text>
        </View>
        <View style={[style.card, { minWidth: 101, minHeight: 101 }]}>
          <Text style={[style.titleCard, { color: '#FF5C3A' }]}>50%</Text>
          <Text style={style.subTitleCard}>Bonus</Text>
        </View>
      </View>
      <Text style={style.titleWrapperdCard}>Earning</Text>
      <View style={[style.wrapperCard, { marginBottom: 70 }]}>
        <View style={style.card}>
          <Text style={style.titleCard}>22 Aug, 2020</Text>
          <Text style={style.subTitleCard}>Join Date</Text>
        </View>
        <View style={style.card}>
          <Text style={style.titleCard}>$2500.00</Text>
          <Text style={style.subTitleCard}>Net Salary</Text>
        </View>
      </View>
    </ScrollView>
  );
};

export default Profile;
