import * as STATE from 'ca-module-login/loginInitialState';
import * as CONST from 'ca-module-login/loginConstant';
import _ from 'lodash';

const loginInitialState = {
  ...STATE.setLoginInitialState,
  ...STATE.setLogoutInitialState,
  action: '',
};

export const loginReducer = (state = loginInitialState, action) => {
  const { payload, type } = action;
  const actions = {
    [CONST.SET_LOGIN]: () => ({
      ...state,
      setLoginParam: payload,
      setLoginFetch: true,
      action: type,
    }),
    [CONST.SET_LOGIN_SUCCESS]: () => ({
      ...state,
      setLoginResponse: payload,
      setLoginFetch: false,
      action: type,
    }),
    [CONST.SET_LOGIN_FAILED]: () => ({
      ...state,
      setLoginFailed: payload,
      setLoginFetch: false,
      action: type,
    }),
    [CONST.SET_LOGIN_CLEAR]: () => ({
      ...state,
      ...STATE.setLoginInitialState,
      action: type,
    }),

    [CONST.SET_LOGOUT]: () => ({
      ...state,
      setLogoutParam: payload,
      setLogoutFetch: true,
      action: type,
    }),
    [CONST.SET_LOGOUT_SUCCESS]: () => ({
      ...state,
      setLogoutResponse: payload,
      setLogoutFetch: false,
      action: type,
    }),
    [CONST.SET_LOGOUT_FAILED]: () => ({
      ...state,
      setLogoutFailed: payload,
      setLogoutFetch: false,
      action: type,
    }),
    [CONST.SET_LOGOUT_CLEAR]: () => ({
      ...state,
      ...STATE.setLogoutInitialState,
      action: type,
    }),

    DEFAULT: () => state,
  };
  return (actions[type] || actions.DEFAULT)();
};
