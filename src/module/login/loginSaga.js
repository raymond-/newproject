import { takeLatest, put, call } from 'redux-saga/effects';
import { RESPONSE_STATUS } from 'ca-util/constant';
import * as CONST from 'ca-module-login/loginConstant';
import {
  setLoginSuccess,
  setLoginFailed,
  setLogoutFailed,
  setLogoutSuccess,
} from 'ca-module-login/loginAction';
import { setLoginAPI, setLogoutAPI } from './loginApi';

function* setLogin(params) {
  try {
    const response = yield call(setLoginAPI, params.payload);
    yield put(setLoginSuccess(response.data));
  } catch (error) {
    switch (error?.response?.status) {
      case RESPONSE_STATUS.BAD_REQUEST:
        yield put(setLoginFailed(error?.response?.data));
        break;
      case RESPONSE_STATUS.ERROR:
        yield put(setLoginFailed(error?.response?.data));
        break;
      default:
    }
  }
}

function* setLogout(params) {
  try {
    const response = yield call(setLogoutAPI, params.payload);
    yield put(setLogoutSuccess(response.data));
  } catch (error) {
    switch (error?.response?.status) {
      case RESPONSE_STATUS.BAD_REQUEST:
        yield put(setLogoutFailed(error?.response?.data));
        break;
      case RESPONSE_STATUS.ERROR:
        yield put(setLogoutFailed(error?.response?.data));
        break;
      default:
    }
  }
}

export default [
  takeLatest(CONST.SET_LOGIN, setLogin),
  takeLatest(CONST.SET_LOGOUT, setLogout),
];
