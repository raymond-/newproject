import * as CONST from 'ca-module-login/loginConstant';

export const setLogin = (payload) => ({
  type: CONST.SET_LOGIN,
  payload,
});
export const setLoginSuccess = (payload) => ({
  type: CONST.SET_LOGIN_SUCCESS,
  payload,
});
export const setLoginFailed = (payload) => ({
  type: CONST.SET_LOGIN_FAILED,
  payload,
});
export const setLoginClear = (payload) => ({
  type: CONST.SET_LOGIN_CLEAR,
  payload,
});

export const setLogout = (payload) => ({
  type: CONST.SET_LOGOUT,
  payload,
});
export const setLogoutSuccess = (payload) => ({
  type: CONST.SET_LOGOUT_SUCCESS,
  payload,
});
export const setLogoutFailed = (payload) => ({
  type: CONST.SET_LOGOUT_FAILED,
  payload,
});
export const setLogoutClear = (payload) => ({
  type: CONST.SET_LOGOUT_CLEAR,
  payload,
});
