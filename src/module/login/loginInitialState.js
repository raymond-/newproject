export const setLoginInitialState = {
  setLoginFetch: false,
  setLoginParam: {},
  setLoginResponse: null,
  setLoginFailed: {
    message: '',
  },
};

export const setLogoutInitialState = {
  setLogoutFetch: false,
  setLogoutParam: {},
  setLogoutResponse: null,
  setLogoutFailed: {
    message: '',
  },
};
