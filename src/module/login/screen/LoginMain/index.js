import { connect } from 'react-redux';
import { setIsAlreadyLaunched, setLoading } from 'ca-bootstrap/bootstrapAction';
import { setLogin, setLoginClear } from 'ca-module-login/loginAction';
import View from './View';

const mapStateToProps = (state) => ({
  loginAction: state.login.action,
  setLoginResponse: state.login.setLoginResponse,
});

const mapDispatchToProps = {
  setIsAlreadyLaunched: (payload) => setIsAlreadyLaunched(payload),
  setLogin: (payload) => setLogin(payload),
  setLoginClear: () => setLoginClear(),
  setLoading: (payload) => setLoading(payload),
};

export default connect(mapStateToProps, mapDispatchToProps)(View);
