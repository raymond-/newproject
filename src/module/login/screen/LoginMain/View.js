import React, { useState, useEffect, useRef, useCallback } from 'react';
import { View, Text, TouchableOpacity } from 'react-native';
import Input from 'ca-component-generic/Input';
import { NAVIGATION } from 'ca-util/constant';
import PropTypes from 'prop-types';
import { Lock, Mail } from 'ca-config/Svg';
import SplashScreen from 'react-native-splash-screen';
import { useFocusEffect } from '@react-navigation/native';
import { useMount } from 'ca-util/common';
import {
  SET_LOGIN_FAILED,
  SET_LOGIN_SUCCESS,
} from 'ca-module-login/loginConstant';
import style from './style';

function Login(props) {
  const {
    setIsAlreadyLaunched,
    navigation,
    // eslint-disable-next-line no-unused-vars
    lang,
    setLogin,
    loginAction,
    setLoginClear,
    setLoading,
    setLoginResponse,
  } = props;
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const usernameInputRef = useRef(null);
  const passwordInputRef = useRef(null);

  useMount(() => {
    SplashScreen.hide();
  });

  useFocusEffect(
    useCallback(() => {
      setIsAlreadyLaunched(true);
      const unsubscribe = navigation.addListener('focus', () => {
        if (setLoginResponse?.message === 'Logged In') {
          navigation.reset({
            index: 0,
            routes: [{ name: NAVIGATION.TABMAIN.TabMain }],
          });
        }
      });
      return unsubscribe;
    }, [navigation, setIsAlreadyLaunched, setLoginResponse])
  );

  useEffect(() => {
    // Auto-focus the username input when the component mounts
    if (usernameInputRef.current) {
      usernameInputRef.current.focus();
    }
  }, []);

  useEffect(() => {
    actionResult(loginAction);
  }, [actionResult, loginAction]);

  const actionResult = useCallback(
    (act) => {
      if (act === SET_LOGIN_SUCCESS) {
        setLoading(false);
        navigation.replace(NAVIGATION.TABMAIN.TabMain);
      }
      if (act === SET_LOGIN_FAILED) {
        setLoading(false);
        setLoginClear();
        setPassword('');
        setUsername('');
      }
    },
    [navigation, setLoading, setLoginClear]
  );

  const handleLogin = () => {
    setLoading(true);
    setLogin({
      usr: username,
      pwd: password,
    });
  };

  return (
    <View style={style.container}>
      <View style={style.wrapperLogin}>
        <Text style={style.welcome}>Welcome Back!</Text>
        <Text style={style.title}>Sign in continue</Text>
        <Input
          ref={usernameInputRef}
          placeholder="Username"
          value={username}
          onChangeText={setUsername}
          returnKeyType="next"
          icon={<Mail />}
        />
        <Input
          ref={passwordInputRef}
          placeholder="Password"
          value={password}
          secureTextEntry
          onChangeText={setPassword}
          returnKeyType="done"
          onSubmitEditing={handleLogin}
          icon={<Lock />}
        />

        <View style={style.wrapperSession}>
          <Text>Remember me</Text>
          <Text style={style.textForgetPassword}>Forget password?</Text>
        </View>
        <TouchableOpacity style={style.button} onPress={handleLogin}>
          <Text style={style.buttonText}>Sign in</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
}

export default Login;

Login.propTypes = {
  navigation: PropTypes.objectOf(Object).isRequired,
  lang: PropTypes.string.isRequired,
  setIsAlreadyLaunched: PropTypes.func.isRequired,
  loginAction: PropTypes.string.isRequired,
  setLogin: PropTypes.func.isRequired,
  setLoginClear: PropTypes.func.isRequired,
  setLoading: PropTypes.func.isRequired,
  setLoginResponse: PropTypes.string.isRequired,
};
