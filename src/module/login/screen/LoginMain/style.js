export default {
  container: {
    flex: 1,
    backgroundColor: '#F9F9FF',
    padding: 10,
    justifyContent: 'center',
  },
  wrapperLogin: {
    backgroundColor: '#ffffff',
    padding: 20,
    paddingVertical: 30,
    borderRadius: 20,
  },
  title: {
    fontSize: 18,
    marginBottom: 20,
    textAlign: 'center',
    color: 'black',
  },
  input: {
    height: 40,
    borderColor: '#cccccc',
    borderWidth: 1,
    borderRadius: 5,
    marginBottom: 10,
    paddingHorizontal: 10,
  },
  button: {
    backgroundColor: '#1C67F6',
    borderRadius: 25,
    paddingVertical: 15,
  },
  buttonText: {
    color: '#ffffff',
    fontSize: 16,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  welcome: {
    fontSize: 22,
    fontWeight: 'bold',
    marginBottom: 8,
    textAlign: 'center',
    color: '#1C67F6',
  },
  wrapperSession: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  textForgetPassword: {
    color: '#FF5C3A',
    fontSize: 16,
  },
};
