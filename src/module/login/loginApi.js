import { api } from 'ca-bootstrap/bootstrapApi';
import { API } from 'ca-util/constant';

export const setLoginAPI = (payload) => {
  return api.post(API.LOGIN.login, payload);
};

export const setLogoutAPI = (payload) => {
  return api.post(API.LOGIN.logout, payload);
};
