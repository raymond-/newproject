import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NAVIGATION } from 'ca-util/constant';
import { BtnBack } from 'ca-config/Svg';
import { TouchableOpacity } from 'react-native-gesture-handler';
import { headerNavStyle } from 'ca-config/Navbarstyle';
import Color from 'ca-config/Color';
import * as Screen from './screen/index';

const Stack = createStackNavigator();
const { LOGIN } = NAVIGATION;

function LoginStack() {
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        headerTitleAlign: 'center',
        headerTitleStyle: {
          fontWeight: '700',
        },
        headerStyle: {
          height: 70,
        },
        title: 'Sign In',
        headerLeft: () => (
          <TouchableOpacity style={{ padding: 15 }} onPress={() => {}}>
            <BtnBack width={24} height={24} fill={Color.main.dark.white} />
          </TouchableOpacity>
        ),
      }}>
      <Stack.Screen name={LOGIN.LoginMain} component={Screen.LoginMain} />
    </Stack.Navigator>
  );
}

export default LoginStack;
