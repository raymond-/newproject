import React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import { IconMaps } from 'ca-config/Svg';
import propTypes from 'prop-types';
import Color from 'ca-config/Color';

function CardAttendance(props) {
  const { image, checkin, checkout, address, status } = props;

  const statusColorDictionary = {
    Present: 'rgba(28, 103, 246, 1)',
    Absent: 'rgba(255, 162, 0, 1)',
    Late: 'rgba(255, 92, 58, 1)',
  };

  const statusColor = statusColorDictionary[status];

  return (
    <View style={styles.card}>
      <Image style={styles.image} source={{ uri: `${image}` }} />
      <View style={styles.flexColumn}>
        <View style={styles.flexRow}>
          <View style={styles.w30}>
            <Text style={styles.textBlackBold}>Check-in</Text>
            <Text style={[styles.textBlue, styles.textSmall]}>{checkin}</Text>
          </View>
          <View style={styles.w30}>
            <Text style={styles.textBlackBold}>Check-out</Text>
            <Text style={[styles.textRed, styles.textSmall]}>{checkout}</Text>
          </View>

          <View style={styles.w40}>
            {status !== null ? (
              <View
                style={[
                  styles.containerStatus,
                  { backgroundColor: statusColor },
                ]}>
                <Text style={styles.textStatus}>{status}</Text>
              </View>
            ) : null}
          </View>
        </View>
        <View style={[styles.flexRow, styles.mT8]}>
          <IconMaps />
          <Text style={styles.mL10}>{address}</Text>
        </View>
      </View>
    </View>
  );
}

export default CardAttendance;

CardAttendance.defaultProps = {
  image: null,
  checkin: null,
  checkout: null,
  address: null,
  status: null,
};

CardAttendance.propTypes = {
  image: propTypes.string,
  checkin: propTypes.string,
  checkout: propTypes.string,
  address: propTypes.string,
  status: propTypes.string,
};

const styles = StyleSheet.create({
  // eslint-disable-next-line react-native/no-color-literals
  card: {
    marginBottom: 10,
    backgroundColor: 'white',
    width: '100%',
    height: 99,
    borderRadius: 8,
    flexDirection: 'row',
    alignItems: 'center',
    padding: 15,
  },
  image: { width: 65, height: 65, borderRadius: 50, marginRight: 10 },
  textSmall: { fontWeight: '700', fontSize: 14 },
  // eslint-disable-next-line react-native/no-color-literals
  textBlue: { color: 'rgba(28, 103, 246, 1)' },
  // eslint-disable-next-line react-native/no-color-literals
  textRed: { color: 'rgba(255, 92, 58, 1)' },
  textStatus: {
    color: Color.main.light.white,
    fontWeight: '400',
    fontSize: 14,
    marginBottom: 4,
  },
  flexColumn: {
    flexDirection: 'column',
  },
  flexRow: {
    flexDirection: 'row',
  },
  w30: { width: '30%' },
  w40: { width: '40%' },
  textBlackBold: {
    color: Color.main.light.black,
    fontWeight: '700',
    fontSize: 14,
  },
  containerStatus: {
    width: 55,
    height: 25,
    borderRadius: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  mT8: { marginTop: 8 },
  mL10: { marginLeft: 10 },
});
