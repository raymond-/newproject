import * as CONST from 'ca-module-attendance/attendanceConstant';

export const getAttendance = (payload) => ({
  type: CONST.GET_ATTENDANCE,
  payload,
});
export const getAttendanceSuccess = (payload) => ({
  type: CONST.GET_ATTENDANCE_SUCCESS,
  payload,
});
export const getAttendanceFailed = (payload) => ({
  type: CONST.GET_ATTENDANCE_FAILED,
  payload,
});
export const getAttendanceClear = (payload) => ({
  type: CONST.GET_ATTENDANCE_CLEAR,
  payload,
});
