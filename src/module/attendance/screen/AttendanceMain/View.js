import React, { useState, useEffect } from 'react';
import { Text, View, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import { IconDate } from 'ca-config/Svg';
import moment from 'moment';
import Padder from 'ca-component-container/Padder';
import CardAttendance from 'ca-module-attendance/component/CardAttendance';
import { TouchableOpacity } from 'react-native-gesture-handler';
import style from './style';

function AttendanceMain(props) {
  const {
    // navigation,
    // lang,
    // colorScheme,
    getAttendance,
    getAttendanceResponse,
  } = props;

  const [selectedButton, setSelectedButton] = useState('Present');

  useEffect(() => {
    getAttendance();
    return () => {};
  }, [getAttendance]);

  const menuAttendance = [
    {
      id: 1,
      status: 'Present',
      amount: 20,
    },
    {
      id: 2,
      status: 'Late',
      amount: 20,
    },
    {
      id: 3,
      status: 'Absent',
      amount: 20,
    },
  ];

  const handleButtonPress = (buttonName) => {
    setSelectedButton(buttonName);
  };

  const dataPresent = getAttendanceResponse?.data?.filter(
    (data) => data.status === 'Present'
  );
  const dataLate = getAttendanceResponse?.data?.filter(
    (data) => data.status === 'Late'
  );
  const dataAbsent = getAttendanceResponse?.data?.filter(
    (data) => data.status === 'Absent'
  );

  let bgButton;
  let textAmount;
  let containerAmount;
  let textStatus;
  let showData;

  switch (selectedButton) {
    case 'Present':
      bgButton = style.bgBlue;
      textAmount = style.textWhite;
      containerAmount = style.bgBlueActive;
      textStatus = style.textWhite;
      showData = dataPresent;

      break;
    case 'Late':
      bgButton = style.bgOrange;
      textAmount = style.textWhite;
      containerAmount = style.bgOrangeActive;
      textStatus = style.textWhite;
      showData = dataLate;

      break;
    case 'Absent':
      bgButton = style.bgRed;
      textAmount = style.textWhite;
      containerAmount = style.bgRedActive;
      textStatus = style.textWhite;
      showData = dataAbsent;
      break;
    default:
      bgButton = style.bgBlue;
      textAmount = style.textWhite;
      containerAmount = style.bgBlueActive;
      textStatus = style.textWhite;
      showData = dataPresent;
      break;
  }
  return (
    <ScrollView>
      <Padder>
        <View style={style.containerContentTop}>
          <Text style={style.textDate}>Today Attendance</Text>
          <IconDate />
        </View>
        <View style={style.containerButton}>
          {menuAttendance.map((item) => (
            <TouchableOpacity
              style={[
                style.button,
                selectedButton === item.status ? bgButton : style.bgWhite,
              ]}
              onPress={() => handleButtonPress(item.status)}>
              <View
                style={[
                  style.containerAmount,
                  selectedButton === item.status
                    ? containerAmount
                    : style.bgOrangeInactive,
                ]}>
                <Text
                  style={[
                    style.textAmount,
                    selectedButton === item.status
                      ? textAmount
                      : style.textBlack,
                  ]}>
                  {' '}
                  {item.amount}
                </Text>
              </View>
              <Text
                style={[
                  style.textStatus,
                  selectedButton === item.status ? textStatus : style.textBlack,
                ]}>
                {item.status}
              </Text>
            </TouchableOpacity>
          ))}
        </View>

        {showData?.map((data) => (
          <CardAttendance
            key={data.id}
            image="https://picsum.photos/seed/picsum/200/300"
            checkin={moment(data.in_time).format('HH:mm a').toUpperCase()}
            checkout={moment(data.out_time).format('HH:mm a').toUpperCase()}
            address={data.shift}
            status={data.status}
          />
        ))}
      </Padder>
    </ScrollView>
  );
}

export default AttendanceMain;

AttendanceMain.defaultProps = {
  // colorScheme: 'light',
};

AttendanceMain.propTypes = {
  // navigation: PropTypes.objectOf(Object).isRequired,
  // lang: PropTypes.string.isRequired,
  // colorScheme: PropTypes.string,
  getAttendance: PropTypes.func.isRequired,
  getAttendanceResponse: PropTypes.objectOf(Array).isRequired,
};
