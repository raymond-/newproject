import { connect } from 'react-redux';
import { getAttendance } from 'ca-module-attendance/attendanceAction';
import View from './View';

const mapStateToProps = (state) => ({
  lang: state.auth.lang,
  getAttendanceResponse: state.attendance.getAttendanceResponse,
});

const mapDispatchToProps = {
  getAttendance: (payload) => getAttendance(payload),
};

export default connect(mapStateToProps, mapDispatchToProps)(View);
