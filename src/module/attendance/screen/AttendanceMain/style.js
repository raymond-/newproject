export default {
  containerContentTop: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  textBlue: {
    color: 'rgba(28, 103, 246, 1)',
  },
  textWhite: {
    color: 'white',
  },
  textBlack: {
    color: 'rgba(25, 28, 36, 1)',
  },
  textOrange: {
    color: 'rgba(255, 162, 0, 1)',
  },
  textRed: {
    color: 'rgba(255, 92, 58, 1)',
  },
  bgBlueActive: {
    backgroundColor: '#3376f7',
  },
  bgOrangeActive: {
    backgroundColor: '#FFAB19',
  },
  bgRedActive: {
    backgroundColor: '#FF6C4E',
  },
  bgBlueInactive: {
    backgroundColor: '#E8F0FE',
  },
  bgOrangeInactive: {
    backgroundColor: '#FFF6E5',
  },
  bgRedInactive: {
    backgroundColor: '#FFEFEB',
  },
  bgBlue: {
    backgroundColor: 'rgba(28, 103, 246, 1)',
  },
  bgWhite: {
    backgroundColor: 'white',
  },
  bgOrange: {
    backgroundColor: 'rgba(255, 162, 0, 1)',
  },
  bgRed: {
    backgroundColor: 'rgba(255, 92, 58, 1)',
  },
  textDate: {
    color: 'rgba(25, 28, 36, 1)',
    fontSize: 18,
    fontWeight: '700',
  },
  containerButton: {
    marginVertical: 20,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  button: {
    backgroundColor: 'rgba(28, 103, 246, 1)',
    width: 101,
    height: 120,
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  containerAmount: {
    height: 60,
    width: 60,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textAmount: {
    fontSize: 24,
    fontWeight: '700',
    marginRight: 8,
  },
  textStatus: {
    marginTop: 5,
    fontWeight: '700',
    fontSize: 14,
  },
};
