export const getAttendanceInitialState = {
  getAttendanceFetch: false,
  getAttendanceParam: {},
  getAttendanceResponse: null,
  getAttendanceFailed: {
    message: '',
  },
};
