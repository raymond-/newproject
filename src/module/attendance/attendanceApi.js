import { api } from 'ca-bootstrap/bootstrapApi';
import { API } from 'ca-util/constant';

export const getAttendanceAPI = (payload) => {
  return api.get(API.ATTENDANCE.attendance, payload);
};
