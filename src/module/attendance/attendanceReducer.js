import * as STATE from 'ca-module-attendance/attendanceInitialState';
import * as CONST from 'ca-module-attendance/attendanceConstant';
import _ from 'lodash';

const attendanceInitialState = {
  ...STATE.getAttendanceInitialState,
  action: '',
};

export const attendanceReducer = (state = attendanceInitialState, action) => {
  const { payload, type } = action;
  const actions = {
    [CONST.GET_ATTENDANCE]: () => ({
      ...state,
      getAttendanceParam: payload,
      getAttendanceFetch: true,
      action: type,
    }),
    [CONST.GET_ATTENDANCE_SUCCESS]: () => ({
      ...state,
      getAttendanceResponse: payload,
      getAttendanceFetch: false,
      action: type,
    }),
    [CONST.GET_ATTENDANCE_FAILED]: () => ({
      ...state,
      getAttendanceFailed: payload,
      getAttendanceFetch: false,
      action: type,
    }),
    [CONST.GET_ATTENDANCE_CLEAR]: () => ({
      ...state,
      ...STATE.getAttendanceInitialState,
      action: type,
    }),

    DEFAULT: () => state,
  };
  return (actions[type] || actions.DEFAULT)();
};
