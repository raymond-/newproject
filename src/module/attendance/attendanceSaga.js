import { takeLatest, put, call } from 'redux-saga/effects';
import { RESPONSE_STATUS } from 'ca-util/constant';
import * as CONST from 'ca-module-attendance/attendanceConstant';
import { getAttendanceFailed, getAttendanceSuccess } from './attendanceAction';
import { getAttendanceAPI } from './attendanceApi';

function* getAttendance(params) {
  try {
    const response = yield call(getAttendanceAPI, params.payload);
    yield put(getAttendanceSuccess(response.data));
  } catch (error) {
    switch (error?.response?.status) {
      case RESPONSE_STATUS.BAD_REQUEST:
        yield put(getAttendanceFailed(error?.response?.data));
        break;
      case RESPONSE_STATUS.ERROR:
        yield put(getAttendanceFailed(error?.response?.data));
        break;
      default:
    }
  }
}

export default [takeLatest(CONST.GET_ATTENDANCE, getAttendance)];
