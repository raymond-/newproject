import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NAVIGATION } from 'ca-util/constant';
import { BtnBack } from 'ca-config/Svg';
import Color from 'ca-config/Color';
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as Screen from './screen/index.';

const Stack = createStackNavigator();
const { ATTENDANCE, TABMAIN } = NAVIGATION;

function AttendanceMainStack(props) {
  const { navigation, lang, colorScheme } = props;

  const handleBackHome = () => {
    navigation.replace(TABMAIN.TabMain);
  };
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        headerTitleAlign: 'center',
        headerTitleStyle: {
          fontWeight: '700',
        },
        headerStyle: {
          height: 100,
        },
        title: 'Attandance',
        headerLeft: () => (
          <TouchableOpacity style={{ padding: 15 }} onPress={handleBackHome}>
            <BtnBack width={24} height={24} fill={Color.main.dark.white} />
          </TouchableOpacity>
        ),
      }}>
      <Stack.Screen
        name={ATTENDANCE.AttendanceMain}
        component={Screen.AttendanceMain}
      />
    </Stack.Navigator>
  );
}

export default AttendanceMainStack;
