import * as CONST from 'ca-module-payslip/paySlipConstant';

export const getPaySlip = (payload) => ({
  type: CONST.GET_PAYSLIP,
  payload,
});
export const getPaySlipSuccess = (payload) => ({
  type: CONST.GET_PAYSLIP_SUCCESS,
  payload,
});
export const getPaySlipFailed = (payload) => ({
  type: CONST.GET_PAYSLIP_FAILED,
  payload,
});
export const getPaySlipClear = (payload) => ({
  type: CONST.GET_PAYSLIP_CLEAR,
  payload,
});
