import React from 'react';
import propTypes from 'prop-types';
import { IconAttachment } from 'ca-config/Svg';
import { Text, View, Image, StyleSheet } from 'react-native';

function CardPaySlip(props) {
  const {
    image,
    name,
    dateTransaction,
    transactionName,
    dateNow,
    price,
    status,
  } = props;

  const setDynamicStatusColor = () => {
    const styleStatus = [];
    if (status === 'Cancelled') {
      styleStatus.push(styles.textOrange);
    } else if (status === 'Paid') {
      styleStatus.push(styles.textGreen);
    } else {
      styleStatus.push(styles.textBlue);
    }
    return styleStatus;
  };
  return (
    <View style={styles.container}>
      <View style={styles.card}>
        <Image source={{ uri: `${image}` }} style={styles.image} />
        <View style={styles.contentCenter}>
          <Text style={styles.textName}>{name}</Text>
          <Text style={styles.textDate}>{dateTransaction}</Text>
          <Text style={styles.textTransactionName}>{transactionName}</Text>
        </View>
        <View style={styles.contentRight}>
          <Text
            style={[
              styles.textDate,
              status !== 'Request' ? styles.mb5 : styles.mb20,
            ]}>
            {dateNow}
          </Text>
          <View style={styles.contentRightCenter}>
            <IconAttachment />
            <Text style={styles.textPrice}>{price}</Text>
          </View>
          {status !== 'Request' ? (
            <Text style={[styles.textStatus, ...setDynamicStatusColor()]}>
              {status}
            </Text>
          ) : null}
        </View>
      </View>
    </View>
  );
}

export default CardPaySlip;

CardPaySlip.defaultProps = {
  image: null,
  name: null,
  dateTransaction: null,
  transactionName: null,
  dateNow: null,
  price: null,
  status: null,
};

CardPaySlip.propTypes = {
  image: propTypes.string,
  name: propTypes.string,
  dateTransaction: propTypes.string,
  transactionName: propTypes.string,
  dateNow: propTypes.string,
  price: propTypes.string,
  status: propTypes.string,
};

const styles = StyleSheet.create({
  container: {
    marginTop: 20,
  },
  // eslint-disable-next-line react-native/no-color-literals
  card: {
    width: '100%',
    height: 90,
    backgroundColor: 'rgba(255, 255, 255, 1)',
    borderRadius: 10,
    padding: 15,
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    width: 60,
    height: 60,
    borderRadius: 50,
    marginRight: 15,
  },
  contentCenter: { width: '47%' },
  // eslint-disable-next-line react-native/no-color-literals
  textName: {
    fontSize: 14,
    fontWeight: '700',
    color: 'rgba(25, 28, 36, 1)',
    marginBottom: 5,
  },
  // eslint-disable-next-line react-native/no-color-literals
  textDate: {
    fontSize: 12,
    fontWeight: '400',
    color: 'rgba(166, 163, 184, 1)',
  },
  // eslint-disable-next-line react-native/no-color-literals
  textTransactionName: {
    fontSize: 12,
    fontWeight: '700',
    color: 'rgba(28, 103, 246, 1)',
  },
  contentRight: { alignItems: 'flex-end', width: '30%' },
  contentRightCenter: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 2,
  },
  // eslint-disable-next-line react-native/no-color-literals
  textPrice: {
    fontSize: 12,
    fontWeight: '700',
    color: 'rgba(255, 92, 58, 1)',
    marginLeft: 5,
  },
  // eslint-disable-next-line react-native/no-color-literals
  textStatus: {
    fontSize: 12,
    fontWeight: '700',
  },
  mb5: {
    marginBottom: 5,
  },
  mb20: {
    marginBottom: 20,
  },
  // eslint-disable-next-line react-native/no-color-literals
  textGreen: {
    color: 'rgba(77, 185, 72, 1)',
  },
  // eslint-disable-next-line react-native/no-color-literals
  textOrange: {
    color: 'rgba(255, 162, 0, 1)',
  },
  // eslint-disable-next-line react-native/no-color-literals
  textBlue: {
    color: 'rgba(28, 103, 246, 1)',
  },
});
