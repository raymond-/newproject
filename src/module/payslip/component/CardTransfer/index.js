import React from 'react';
import { Text, View, Image, StyleSheet } from 'react-native';
import { IconArrowOrange } from 'ca-config/Svg';
import propTypes from 'prop-types';

function CardTransfer(props) {
  const { image, transactionName, accoundId, price } = props;
  return (
    <View style={styles.card}>
      <Image
        source={{
          uri: `${image}`,
        }}
        style={styles.image}
      />
      <View style={styles.contentCenter}>
        <Text style={styles.text}>{transactionName}</Text>
        <Text>{accoundId}</Text>
      </View>
      <View style={styles.contentRight}>
        <Text style={styles.textPrice}>{price}</Text>
        <IconArrowOrange />
      </View>
    </View>
  );
}

export default CardTransfer;

CardTransfer.defaultProps = {
  image: null,
  transactionName: null,
  accoundId: null,
  price: null,
};

CardTransfer.propTypes = {
  image: propTypes.string,
  accoundId: propTypes.string,
  transactionName: propTypes.string,
  price: propTypes.string,
};

const styles = StyleSheet.create({
  // eslint-disable-next-line react-native/no-color-literals
  card: {
    height: 75,
    width: '100%',
    flexDirection: 'row',
    padding: 18,
    backgroundColor: 'white',
    borderRadius: 10,
    alignItems: 'center',
    marginBottom: 10,
  },
  image: { height: 47, width: 47, borderRadius: 50, marginRight: 15 },
  contentCenter: { flexDirection: 'column', width: '47%' },
  text: { marginBottom: 8, fontSize: 16, fontWeight: '700' },
  contentRight: {
    flexDirection: 'column',
    width: '30%',
    alignItems: 'flex-end',
  },
  // eslint-disable-next-line react-native/no-color-literals
  textPrice: {
    marginBottom: 8,
    color: 'rgba(255, 92, 58, 1)',
    fontSize: 14,
    fontWeight: '500',
  },
});
