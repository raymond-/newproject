import { takeLatest, put, call } from 'redux-saga/effects';
import { RESPONSE_STATUS } from 'ca-util/constant';
import * as CONST from 'ca-module-payslip/paySlipConstant';
import { getPaySlipFailed, getPaySlipSuccess } from './paySlipAction';
import { getPaySlipAPI } from './paySlipApi';

function* getPaySlip(params) {
  try {
    const response = yield call(getPaySlipAPI, params.payload);
    yield put(getPaySlipSuccess(response.data));
  } catch (error) {
    switch (error?.response?.status) {
      case RESPONSE_STATUS.BAD_REQUEST:
        yield put(getPaySlipFailed(error?.response?.data));
        break;
      case RESPONSE_STATUS.ERROR:
        yield put(getPaySlipFailed(error?.response?.data));
        break;
      default:
    }
  }
}

export default [takeLatest(CONST.GET_PAYSLIP, getPaySlip)];
