import * as STATE from 'ca-module-payslip/paySlipInitialState';
import * as CONST from 'ca-module-payslip/paySlipConstant';
import _ from 'lodash';

const paySlipInitialState = {
  ...STATE.getPaySlipInitialState,
  action: '',
};

export const paySlipReducer = (state = paySlipInitialState, action) => {
  const { payload, type } = action;
  const actions = {
    [CONST.GET_PAYSLIP]: () => ({
      ...state,
      getPaySlipParam: payload,
      getPaySlipFetch: true,
      action: type,
    }),
    [CONST.GET_PAYSLIP_SUCCESS]: () => ({
      ...state,
      getPaySlipResponse: payload,
      getPaySlipFetch: false,
      action: type,
    }),
    [CONST.GET_PAYSLIP_FAILED]: () => ({
      ...state,
      getPaySlipFailed: payload,
      getPaySlipFetch: false,
      action: type,
    }),
    [CONST.GET_PAYSLIP_CLEAR]: () => ({
      ...state,
      ...STATE.getPaySlipInitialState,
      action: type,
    }),

    DEFAULT: () => state,
  };
  return (actions[type] || actions.DEFAULT)();
};
