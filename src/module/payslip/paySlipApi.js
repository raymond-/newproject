import { api } from 'ca-bootstrap/bootstrapApi';
import { API } from 'ca-util/constant';

export const getPaySlipAPI = (payload) => {
  return api.get(API.PAYSLIP.payslip, payload);
};
