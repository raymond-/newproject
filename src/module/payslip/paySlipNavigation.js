import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import { NAVIGATION } from 'ca-util/constant';
import { BtnBack } from 'ca-config/Svg';
import Color from 'ca-config/Color';
import { TouchableOpacity } from 'react-native-gesture-handler';
import * as Screen from './screen/index';

const Stack = createStackNavigator();
const { PAYSLIP, TABMAIN } = NAVIGATION;

export function PaySlipMainStack(props) {
  const { navigation, lang, colorScheme } = props;

  const handleBackHome = () => {
    navigation.replace(TABMAIN.TabMain);
  };

  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        headerTitleAlign: 'center',
        headerTitleStyle: {
          fontWeight: '700',
        },
        headerStyle: {
          height: 100,
        },
        title: 'Pay Slip',
        headerLeft: () => (
          <TouchableOpacity style={{ padding: 15 }} onPress={handleBackHome}>
            <BtnBack width={24} height={24} fill={Color.main.dark.white} />
          </TouchableOpacity>
        ),
      }}>
      <Stack.Screen name={PAYSLIP.PaySlipMain} component={Screen.PaySlipMain} />
    </Stack.Navigator>
  );
}

export function PaySlipDetilStack(props) {
  const { navigation, lang, colorScheme } = props;
  const handlePreviousPage = () => {
    navigation.navigate(PAYSLIP.PaySlip);
  };
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        headerTitleAlign: 'center',
        headerTitleStyle: {
          fontWeight: '700',
        },
        headerStyle: {
          height: 100,
        },
        title: 'Payment Details',
        headerLeft: () => (
          <TouchableOpacity
            style={{ padding: 15 }}
            onPress={handlePreviousPage}>
            <BtnBack width={24} height={24} fill={Color.main.dark.white} />
          </TouchableOpacity>
        ),
      }}>
      <Stack.Screen
        name={PAYSLIP.PaySlipDetil}
        component={Screen.PaySlipDetil}
      />
    </Stack.Navigator>
  );
}

export function PaymentProcessStack(props) {
  const { navigation, lang, colorScheme } = props;
  const handlePreviousPage = () => {
    navigation.navigate(PAYSLIP.PaySlipDetil);
  };
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        headerTitleAlign: 'center',
        headerTitleStyle: {
          fontWeight: '700',
        },
        headerStyle: {
          height: 100,
        },
        title: 'Payment',
        headerLeft: () => (
          <TouchableOpacity
            style={{ padding: 15 }}
            onPress={handlePreviousPage}>
            <BtnBack width={24} height={24} fill={Color.main.dark.white} />
          </TouchableOpacity>
        ),
      }}>
      <Stack.Screen
        name={PAYSLIP.PaymentProcess}
        component={Screen.PaymentProcess}
      />
    </Stack.Navigator>
  );
}

export function PaymentFinishStack(props) {
  const { navigation, lang, colorScheme } = props;
  const handlePreviousPage = () => {
    navigation.replace(PAYSLIP.PaymentProcess);
  };
  return (
    <Stack.Navigator
      screenOptions={{
        headerShown: true,
        headerTitleAlign: 'center',
        headerTitleStyle: {
          fontWeight: '700',
        },
        headerStyle: {
          height: 100,
        },
        title: 'Payment Methode',
        headerLeft: () => (
          <TouchableOpacity
            style={{ padding: 15 }}
            onPress={handlePreviousPage}>
            <BtnBack width={24} height={24} fill={Color.main.dark.white} />
          </TouchableOpacity>
        ),
      }}>
      <Stack.Screen
        name={PAYSLIP.PaymentFinish}
        component={Screen.PaymentFinish}
      />
    </Stack.Navigator>
  );
}
