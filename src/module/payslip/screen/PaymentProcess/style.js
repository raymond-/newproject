export default {
  card: {
    marginTop: 20,
    width: '100%',
    height: 322,
    backgroundColor: 'rgba(255, 255, 255, 1)',
    borderRadius: 10,
    alignItems: 'center',
    padding: 20,
  },
  image: {
    height: 100,
    width: 100,
    borderRadius: 50,
    marginBottom: 18,
  },
  textName: {
    fontSize: 18,
    fontWeight: '500',
    color: 'rgba(9, 47, 87, 1)',
    marginBottom: 8,
  },
  containerPrice: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textAmount: {
    fontSize: 16,
    fontWeight: '500',
    color: 'rgba(9, 47, 87, 1)',
  },
  textPrice: {
    fontSize: 16,
    fontWeight: '500',
    color: 'rgba(239, 49, 35, 1)',
    marginLeft: 5,
  },
  button: {
    width: '100%',
    height: 45,
    backgroundColor: 'rgba(28, 103, 246, 1)',
    borderRadius: 40,
    justifyContent: 'center',
    marginTop: 18,
  },
  textButton: {
    textAlign: 'center',
    fontWeight: '700',
    color: 'white',
    fontSize: 16,
  },
  textTransfer: {
    marginVertical: 20,
    fontWeight: '700',
    fontSize: 18,
    color: 'rgba(9, 47, 87, 1)',
  },
};
