import React from 'react';
import { Text, View, Image, TouchableOpacity, ScrollView } from 'react-native';
import Padder from 'ca-component-container/Padder';

import { NAVIGATION } from 'ca-util/constant';
import CardTransfer from 'ca-module-payslip/component/CardTransfer';
import style from './style';

const { PAYSLIP } = NAVIGATION;

function PaymentProcess(props) {
  const { navigation, lang, colorScheme } = props;

  const handleSendNow = () => {
    navigation.navigate(PAYSLIP.PaymentFinish);
  };

  const dummyDataTransfer = [
    {
      id: 1,
      image: 'https://picsum.photos/seed/picsum/200/300',
      transactionName: 'Bussiness Trip',
      price: '$300.00',
      accountId: '236743897329',
    },
    {
      id: 2,
      image: 'https://picsum.photos/seed/picsum/200/300',
      transactionName: 'Bussiness Trip',
      price: '$300.00',
      accountId: '236743897329',
    },
    {
      id: 3,
      image: 'https://picsum.photos/seed/picsum/200/300',
      transactionName: 'Bussiness Trip',
      price: '$300.00',
      accountId: '236743897329',
    },
    {
      id: 4,
      image: 'https://picsum.photos/seed/picsum/200/300',
      transactionName: 'Bussiness Trip',
      price: '$300.00',
      accountId: '236743897329',
    },
    {
      id: 5,
      image: 'https://picsum.photos/seed/picsum/200/300',
      transactionName: 'Bussiness Trip',
      price: '$300.00',
      accountId: '236743897329',
    },
  ];
  return (
    <Padder>
      <View style={style.card}>
        <Image
          source={{
            uri: 'https://picsum.photos/seed/picsum/200/300',
          }}
          style={style.image}
        />

        <Text style={style.textName}>Alexandree</Text>

        <View style={style.containerPrice}>
          <Text style={style.textAmount}>Amount</Text>
          <Text style={style.textPrice}>$ 200.000</Text>
        </View>
        <Text style={style.textAmount}>Bangladesh- Direct to Bank</Text>
        <Text style={style.textAmount}>AC NO: 12543686242424</Text>

        <TouchableOpacity onPress={handleSendNow} style={style.button}>
          <Text style={style.textButton}>Send Now</Text>
        </TouchableOpacity>
      </View>
      <Text style={style.textTransfer}>Recent Transfers :</Text>
      <ScrollView>
        {dummyDataTransfer.map((data) => (
          <CardTransfer
            key={data.id}
            transactionName={data.transactionName}
            accoundId={data.accountId}
            image={data.image}
            price={data.price}
          />
        ))}
      </ScrollView>
    </Padder>
  );
}

export default PaymentProcess;
