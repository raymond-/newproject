import React, { useState } from 'react';
import { Text, View, Image, TouchableOpacity, Modal } from 'react-native';
import { NAVIGATION } from 'ca-util/constant';

import Padder from 'ca-component-container/Padder';
import { IconAttachment } from 'ca-config/Svg';
import style from './style';

const { PAYSLIP } = NAVIGATION;

function PaySlipDetil(props) {
  const { navigation, lang, colorScheme } = props;

  const [modalVisible, setModalVisible] = useState(false);

  const openModal = () => {
    setModalVisible(true);
  };

  const closeModal = () => {
    setModalVisible(false);
  };

  const handleProcessPayment = () => {
    setModalVisible(false);
    navigation.navigate(PAYSLIP.PaymentProcess);
  };
  return (
    <Padder>
      <View style={style.container}>
        <View style={style.contentTop}>
          <Image
            source={{
              uri: 'https://picsum.photos/seed/picsum/200/300',
            }}
            style={style.image}
          />
          <View style={style.contentTopCenter}>
            <Text style={style.textName}>Alexandree</Text>
            <Text style={style.textDate}>27 Aug- 28 Aug, 2021</Text>
            <Text style={style.textTransactionName}>Bussiness Trip</Text>
          </View>
          <View style={style.contentTopRight}>
            <Text style={style.textDate}>2 Augus 2021</Text>
            <View style={style.contentTopRightBottom}>
              <IconAttachment />
              <Text style={style.textPrice}>$ 200.000</Text>
            </View>
          </View>
        </View>
        <View style={style.devider} />
        <View style={style.margin20}>
          <Text style={style.textBottom}>Support Document</Text>
          <View style={style.containerDocument} />
          <TouchableOpacity onPress={openModal} style={style.button}>
            <Text style={style.textButton}>Pay Now</Text>
          </TouchableOpacity>
        </View>
      </View>
      <Modal
        visible={modalVisible}
        animationType="fade"
        transparent
        onRequestClose={closeModal}>
        <View style={style.contentModal}>
          <View style={style.cardContentModal}>
            <Text style={style.textCenter}>Are you sure</Text>
            <Text style={style.textCenter}>you will pay this bill?</Text>
            <View style={style.containerButtonModal}>
              <TouchableOpacity
                style={[style.buttonModal, style.bgOrange]}
                onPress={closeModal}>
                <Text style={style.textCancel}>Cancel</Text>
              </TouchableOpacity>
              <TouchableOpacity
                onPress={handleProcessPayment}
                style={[style.buttonModal, style.bgBlue]}>
                <Text style={style.textPay}>Pay</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    </Padder>
  );
}

export default PaySlipDetil;
