export { default as PaySlipMain } from './PaySlipMain';
export { default as PaySlipDetil } from './PaySlipDetil';
export { default as PaymentProcess } from './PaymentProcess';
export { default as PaymentFinish } from './PaymentFinish';
