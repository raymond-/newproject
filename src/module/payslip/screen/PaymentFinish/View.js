import React from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Padder from 'ca-component-container/Padder';
import { IconSuccess } from 'ca-config/Svg';
import { NAVIGATION } from 'ca-util/constant';
import style from './style';

const { TABMAIN } = NAVIGATION;

function PaymentFinish(props) {
  const { navigation, lang, colorScheme } = props;

  const handleBackHome = () => {
    navigation.replace(TABMAIN.TabMain);
  };
  return (
    <Padder>
      <View style={style.containerIcon}>
        <IconSuccess />
      </View>
      <View style={style.card}>
        <Text style={style.textPayment}>Payment Success</Text>
        <Text style={style.textId}>Tip #ID12345</Text>
        <Text style={style.text}>
          Thank you for choosing our service and trusted to help you with your
          problems
        </Text>
      </View>

      <TouchableOpacity onPress={handleBackHome} style={style.button}>
        <Text style={style.textButton}>Back to Home</Text>
      </TouchableOpacity>
    </Padder>
  );
}

export default PaymentFinish;
