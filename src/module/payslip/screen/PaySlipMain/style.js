export default {
  container: {
    marginTop: 20,
  },
  bgWhite: {
    backgroundColor: 'rgba(255, 255, 255, 1)',
  },
  bgBlue: {
    backgroundColor: 'rgba(28, 103, 246, 1)',
  },
  bgRed: {
    backgroundColor: 'rgba(255, 92, 58, 1)',
  },
  containerStatus: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  buttonTitle: {
    height: 36,
    width: 100,
    borderRadius: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  fontTitleWhite: {
    fontWeight: '500',
    fontSize: 16,
    color: 'rgba(255, 255, 255, 1)',
  },
  fontTitleBlue: {
    fontWeight: '500',
    fontSize: 16,
    color: 'rgba(28, 103, 246, 1)',
  },
};
