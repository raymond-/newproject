import { connect } from 'react-redux';
import { getPaySlip } from 'ca-module-payslip/paySlipAction';
import View from './View';

const mapStateToProps = (state) => ({
  lang: state.auth.lang,
  getPaySlipResponse: state.payslip.getPaySlipResponse,
});

const mapDispatchToProps = {
  getPaySlip: (payload) => getPaySlip(payload),
};

export default connect(mapStateToProps, mapDispatchToProps)(View);
