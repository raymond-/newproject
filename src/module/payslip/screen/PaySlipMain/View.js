import React, { useState, useEffect } from 'react';
import { Text, View, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import { NAVIGATION } from 'ca-util/constant';
import Padder from 'ca-component-container/Padder';
import { TouchableOpacity } from 'react-native-gesture-handler';
import CardPaySlip from 'ca-module-payslip/component/CardPaySlip';
import style from './style';
import locale from './locale';

const { PAYSLIP } = NAVIGATION;

function PaySlipMain(props) {
  const { navigation, lang, colorScheme, getPaySlip, getPaySlipResponse } =
    props;
  const [selectedButton, setSelectedButton] = useState('Requested');

  useEffect(() => {
    getPaySlip();
    return () => {};
  }, [getPaySlip]);

  const handleButtonPress = (buttonName) => {
    setSelectedButton(buttonName);
  };

  const status = [
    {
      title: 'Requested',
      selectedColor: style.bgBlue,
    },
    {
      title: 'Completed',
      selectedColor: style.bgBlue,
    },
    {
      title: 'Cancelled',
      selectedColor: style.bgRed,
    },
  ];

  const dataDummyPaySlip = [
    {
      name: 'Alexandree Agas',
      dateTransaction: '27 Jun - 30 jun, 2021',
      transactionName: 'Bussiness Trip',
      price: '$ 300.00',
      status: 'Paid',
      image: 'https://picsum.photos/seed/picsum/200/300',
    },
    {
      name: 'Alexandree Agas',
      dateTransaction: '27 Jun - 30 jun, 2021',
      transactionName: 'Bussiness Trip',
      price: '$ 300.00',
      status: 'Paid',
      image: 'https://picsum.photos/seed/picsum/200/300',
    },
    {
      name: 'Alexandree Agas',
      dateTransaction: '27 Jun - 30 jun, 2021',
      transactionName: 'Bussiness Trip',
      price: '$ 300.00',
      status: 'Paid',
      image: 'https://picsum.photos/seed/picsum/200/300',
    },
    {
      name: 'Alexandree Agas',
      dateTransaction: '27 Jun - 30 jun, 2021',
      transactionName: 'Bussiness Trip',
      price: '$ 300.00',
      status: 'Paid',
      image: 'https://picsum.photos/seed/picsum/200/300',
    },
    {
      name: 'Alexandree Agas',
      dateTransaction: '27 Jun - 30 jun, 2021',
      transactionName: 'Bussiness Trip',
      price: '$ 300.00',
      status: 'Cancelled',
      image: 'https://picsum.photos/seed/picsum/200/300',
    },
    {
      name: 'Alexandree Agas',
      dateTransaction: '27 Jun - 30 jun, 2021',
      transactionName: 'Bussiness Trip',
      price: '$ 300.00',
      status: 'Cancelled',
      image: 'https://picsum.photos/seed/picsum/200/300',
    },
    {
      name: 'Alexandree Agas',
      dateTransaction: '27 Jun - 30 jun, 2021',
      transactionName: 'Bussiness Trip',
      price: '$ 300.00',
      status: 'Request',
      image: 'https://picsum.photos/seed/picsum/200/300',
    },
    {
      name: 'Alexandree Agas',
      dateTransaction: '27 Jun - 30 jun, 2021',
      transactionName: 'Bussiness Trip',
      price: '$ 300.00',
      status: 'Request',
      image: 'https://picsum.photos/seed/picsum/200/300',
    },
  ];

  const dataRequested = dataDummyPaySlip?.filter(
    (data) => data.status === 'Request'
  );
  const dataCancelled = dataDummyPaySlip?.filter(
    (data) => data.status === 'Cancelled'
  );
  const dataPaid = dataDummyPaySlip?.filter((data) => data.status === 'Paid');

  let showDataByStatus;
  switch (selectedButton) {
    case 'Requested':
      showDataByStatus = dataRequested;
      break;
    case 'Completed':
      showDataByStatus = dataPaid;
      break;
    case 'Cancelled':
      showDataByStatus = dataCancelled;
      break;
    default:
      showDataByStatus = dataRequested;
      break;
  }
  const handleDetilPaySlip = () => {
    navigation.navigate(PAYSLIP.PaySlipDetil);
  };
  return (
    <Padder>
      <View style={style.container}>
        {/* <View style={style.containerStatus}>
          {status.map((item) => (
            <TouchableOpacity
              style={[
                style.buttonTitle,
                selectedButton === item.title
                  ? item.selectedColor
                  : style.bgWhite,
              ]}
              onPress={() => handleButtonPress(item?.title)}>
              <Text
                style={
                  selectedButton === item.title
                    ? style.fontTitleWhite
                    : style.fontTitleBlue
                }>
                {item.title}
              </Text>
            </TouchableOpacity>
          ))}
        </View>
        <ScrollView>
          {showDataByStatus.length < 1 ? (
            <Text style={{ textAlign: 'center', marginTop: 50 }}>No Data</Text>
          ) : showDataByStatus !== dataRequested ? (
            showDataByStatus.map((data) => (
              <CardPaySlip
                image={data.image}
                dateNow="29 Jun 2023"
                name={data.name}
                dateTransaction={data.dateTransaction}
                price={data.price}
                transactionName={data.transactionName}
                status={data.status}
              />
            ))
          ) : (
            showDataByStatus.map((data) => (
              <TouchableOpacity onPress={handleDetilPaySlip}>
                <CardPaySlip
                  image={data.image}
                  dateNow="29 Jun 2023"
                  name={data.name}
                  dateTransaction={data.dateTransaction}
                  price={data.price}
                  transactionName={data.transactionName}
                  status={data.status}
                />
              </TouchableOpacity>
            ))
          )}
        </ScrollView> */}
        {getPaySlipResponse?.data?.map((data) => (
          <CardPaySlip
            image="https://picsum.photos/seed/picsum/200/300"
            dateNow={data.posting_date}
            name={data.employee_name}
            // dateTransaction={data.posting_date}
            price={data.gross_pay}
            transactionName={data.status}
            // status={data.status}
          />
        ))}
      </View>
    </Padder>
  );
}

export default PaySlipMain;

PaySlipMain.defaultProps = {
  colorScheme: 'light',
};

PaySlipMain.propTypes = {
  navigation: PropTypes.objectOf(Object).isRequired,
  lang: PropTypes.string.isRequired,
  colorScheme: PropTypes.string,
  getPaySlip: PropTypes.func.isRequired,
  getPaySlipResponse: PropTypes.objectOf(Array).isRequired,
};
